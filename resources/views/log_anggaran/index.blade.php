@extends('config.layout')
@section('menu_title','LOG ANGGARAN')
@section('path1','Log')
@section('path2','Anggaran')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">Log</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                            <div class="form-inline">
                                <div class="form-group">
                                    <select class="form-control select2" id="tahun" width="30%">
                                        @foreach($years as $y)
                                            <option value="{{$y}}" {!! $y == date('Y') ? 'selected' : '' !!}>{{$y}}&nbsp;&nbsp;</option>
                                        @endforeach
                                    </select>
                                </div>
                                &nbsp;
                                <div class="form-group">
                                    <select class="form-control select2" id="subdit" width="50%">
                                        @if(Auth::user()->get_role_name->level == '1')
                                            <option value="" selected>* Semua&nbsp;&nbsp;</option>
                                        @endif
                                        @foreach($subdit as $y)
                                            <option value="{{$y->kode}}">{{$y->kode}} - {{$y->nama}}&nbsp;&nbsp;</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Subdit</th>
                                <th>Kode Program</th>
                                <th>Kegiatan</th>
                                <th>Action</th>
                                <th>Total</th>
                                <th>Referensi Sebelumnya</th>
                                <th>Dibuat Oleh</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('internal_js')
    <script>
        var table;
        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            $('#tahun').change(function (e){
                table.ajax.reload();
            });
            $('#subdit').change(function (e){
                table.ajax.reload();
            });
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('log_anggaran.data')}}',
                    data: function (e) {
                        e.tahun = $('#tahun').val()
                        e.subdit = $('#subdit').val()
                    }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'nama',name :'ms.nama'},
                    {data: 'kode_gl',name :'anggaran_detail.kode_gl'},
                    {data: 'kegiatan',name :'anggaran_detail.kegiatan'},
                    {data: 'action_user',name:'anggaran_log.action_user'},
                    {data: 'perubahan',name:'anggaran_log.perubahan'},
                    {data: 'sebelumnya',name:'anggaran_log.sebelumnya'},
                    {data: 'user',name :'us.nama'},
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })


    </script>
@endsection
