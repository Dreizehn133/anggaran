@extends('config.layout')
@section('menu_title','User Management')
@section('path1','Master')
@section('path2','User')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">User</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                        </div>
                        <div class="pull-right">
                            <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-fall" data-toggle="modal" href="#mymodal">Add New</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Unit Kerja</th>
                                <th>Role</th>
                                <th>Feature</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Menu Role</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                    <div class="modal-body">
                        <div class="form-group" id="role-group">
                            <p class="mg-b-10">Role</p>
                            <select class="form-control select2" id="role" name="role">
                                @foreach($role as $r)
                                    <option value="{{$r->kode}}">{{$r->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="name-group">
                            <p class="mg-b-10">Name</p>
                            <input type="text" class="form-control" name="name" id="name"/>
                            <input type="hidden" name="ids" id="ids"/>
                        </div>
                        <div class="form-group" id="name-group">
                            <p class="mg-b-10">Email</p>
                            <input type="email" class="form-control" name="email" id="email"/>
                        </div>
                        <div class="form-group" id="name-group">
                            <p class="mg-b-10">Password</p>
                            <input type="password" class="form-control" name="password" id="password"/>
                            <small>Kosongkan Ketika Mengedit Jika Password Tidak ingin di Reset!</small>
                        </div>
                        <div class="form-group" id="subdit-group">
                            <p class="mg-b-10">Unit Kerja</p>
                            <select class="form-control select2" id="subdit" name="subdit">
                                @foreach($subdit as $r)
                                    <option value="{{$r->kode}}">{{$r->kode}} - {{$r->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn ripple btn-primary" type="submit" >Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('internal_js')
    <script>
        var table;
        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';

            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('master.user.data')}}',
                    // data: function (e) {
                    //     e.branch = $('#filter-br').val()
                    // }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'nama',name :'users.nama'},
                    {data: 'email',name :'users.email'},
                    {data: 'subdit',name :'ms.nama'},
                    {data: 'role',name :'ur.nama'},
                    {data: 'session', orderable: false, searchable: false},
                    {data: 'action', orderable: false, searchable: false}
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })
        $('#form').submit(function (even) {
            even.preventDefault();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
                {
                    if(!val){
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('master.user.add')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            // reset()
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            $('#mymodal').modal('hide')
                            table.ajax.reload()
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    console.log(key)
                                    console.log(value)
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        function reset(){
            $('#form')[0].reset()
            $('#ids').val('')
        }
        function edit(id){
            $('#ids').val(id)
            $.ajax({
                type: 'GET',
                url: '{{route('master.user.header')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    $('#email').val(data.email)
                    $('#name').val(data.nama)
                    $('#subdit').val(data.kode_subdit).trigger('change')
                    $('#role').val(data.role).trigger('change')
                    $('#mymodal').modal()
                }
            })
        }
        function remove(id){
            swal({
                title: "Are you sure?",
                text: "You will delete data!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('master.user.delete') }}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Success', {timeOut: 5000});
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        $('#mymodal').on('hidden.bs.modal', function (e) {
            // $(this)
            //     .find("input,textarea")
            //     .val('')
            //     .end()
            //     .find("select")
            //     .val('0')
            //     .trigger('change')
            //     .end()
            //     .find("input[type=checkbox]")
            //     .prop("checked", "")
            //     .end();
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            reset();// remove the error text
        });
        $('input:radio[name=title]').click(function (){
            console.log($(this).val())
            if($(this).val() == 1){
                $('#menus').hide()
            }else{
                $('#menus').show()
            }
        })
        function destroySession(id){
            swal({
                title: "Are you sure?",
                text: "You logout this user?!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('master.user.destroy') }}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Success', {timeOut: 5000});
                        // table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }

    </script>
@endsection
