@extends('config.layout')
@section('menu_title','Master')
@section('path1','Master')
@section('path2','Program')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">Program</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                        </div>
                        <div class="pull-right">
                            <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-fall" data-toggle="modal" href="#mymodal">Add New</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Program</th>
                                <th>Nama Program</th>
                                <th>Flag</th>
                                <th>Add By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    @livewire('gl-account')--}}
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Program</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                <div class="modal-body">
                        <div class="form-group" id="header_id-group">
                            <p class="mg-b-10">Header</p>
                            <select class="form-control select2" id="header_id" name="header_id">
                                <option value="0">No Header</option>

                            </select>
                            <input type="hidden" id="ids" name="ids"/>
                            <input type="hidden" id="flag" name="flag" value="off"/>
                        </div>
{{--                    <div class="form-group" id="flag-group">--}}
{{--                        <p class="mb-2">Detail</p>--}}
{{--                        <label class="custom-switch">--}}
{{--                            <input type="checkbox" name="flag" class="custom-switch-input" id="flag">--}}
{{--                            <span class="custom-switch-description">Header</span>&nbsp;--}}
{{--                            <span class="custom-switch-indicator"></span>--}}
{{--                            <span class="custom-switch-description">Detail</span>--}}
{{--                        </label>--}}
{{--                    </div>--}}
                        <div class="form-group" id="kode-group">
                            <p class="mg-b-10">Kode</p>
                            <input type="text" class="form-control" placeholder="Kode Program" id="kode" name="kode" required>
                        </div>
                        <div class="form-group" id="nama-group">
                            <p class="mg-b-10">Nama</p>
                            <input type="text" class="form-control" placeholder="Nama Program" id="nama" name="nama" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn ripple btn-primary" type="submit" >Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('internal_js')
    <script>
        var table;
        $(function (){
            $('#header_id2-group').hide()
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            getHeader();
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('master.gl_account.data')}}?flags=1',
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'kode',name :'m_gl_account.kode'},
                    {data: 'nama',name :'m_gl_account.nama'},
                    {data: 'flag',name :'m_gl_account.flag'},
                    {data: 'user',name :'us.nama'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })
        function getHeader(){
            $.ajax({
                type: 'GET',
                url: '{{route('master.gl_account.header')}}',
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    $('#header_id').find('option').not(':first').remove()
                    $.each(data, function (key, value) {
                        $('#header_id').append('<option value="' + value.id + '">' + value.kode + ' - '+ value.nama + '</option>')
                        // $('#header_id2').append('<option value="' + value.id + '">' + value.kode + ' - '+ value.nama + '</option>')
                    });
                },
                error: function (request, status, error) {
                    console.log(request.responseText)
                    json = $.parseJSON(request.responseText);
                    if (typeof json !== "string") {
                        $.each(json, function (key, value) {
                            $('#' + key + '-group').addClass('has-error');
                            $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                        });
                    } else {
                        toastr.error(request.responseText, error, {timeOut: 5000});

                    }
                }
            });

        }
        $('#form').submit(function (even) {
            even.preventDefault();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
            {
                if(!val){
                    return;
                }
                $('.form-group').removeClass('has-error'); // remove the error class
                $('.help-block').remove(); // remove the error text
                $.ajax({
                    type: 'POST',
                    url: '{{route('master.gl_account.add_data')}}',
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        // reset()
                        toastr.success(data, 'Sukses!', {timeOut: 5000});
                        getHeader();
                        $('#mymodal').modal('hide')
                        table.ajax.reload()
                    },
                    error: function (request, status, error) {
                        var json = JSON.parse(request.responseText);
                        if (typeof json !== "string") {
                            $.each(json, function (key, value) {

                                $('#' + key + '-group').addClass('has-error');
                                $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                            });
                        } else {
                            toastr.error(request.responseText, error, {timeOut: 5000});
                        }
                    }
                });
            }
            );
        });
        function reset(){
            $('#form')[0].reset()
            $('.select2').val('0').trigger('change')
            $('#ids').val('')
            $('#header_id-group').show()
            // $('#header_id2-group').hide()
            $('#flag-group').show()
        }
        function edit(id){
            $('#ids').val(id)
            $.ajax({
                type: 'GET',
                url: '{{route('master.gl_account.header')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    $('#nama').val(data.nama)
                    $('#kode').val(data.kode)
                    $('#header_id').val(data.header_id).trigger('change')
                    // $('#header_id-group').hide()
                    // $('#header_id2-group').hide()
                    // $('#flag').prop('checked', data.flag == 1?false:true);
                    // $('#flag-group').hide()
                    $('#mymodal').modal()
                }
            })
        }
        function remove(id){
            swal({
                title: "Are you sure?",
                text: "You will change status data!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('master.gl_account.delete') }}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Success', {timeOut: 5000});
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        function removePermanent(id){
            swal({
                title: "Are you sure?",
                text: "You will permanent delete data!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('master.gl_account.permanentDelete') }}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Success', {timeOut: 5000});
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        $('#mymodal').on('hidden.bs.modal', function (e) {
            // $(this)
            //     .find("input,textarea")
            //     .val('')
            //     .end()
            //     .find("select")
            //     .val('0')
            //     .trigger('change')
            //     .end()
            //     .find("input[type=checkbox]")
            //     .prop("checked", "")
            //     .end();
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            reset();// remove the error text
        });
        // $('#flag').change(function (){
        //     if($(this).is(':checked')){
        //         $('#header_id-group').hide()
        //         $('#header_id2-group').show()
        //     }else{
        //         $('#header_id-group').show()
        //         $('#header_id2-group').hide()
        //     }
        // })
    </script>
@endsection
