@php
    ini_set('max_execution_time', 0);
@endphp
    <!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preview</title>
    <style>
        .btn-primary {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        .btn-secondary {
            color: #fff;
            background-color: #1c6c40;
            border-color: #1c6c40;
            text-decoration: none;
        }
        .btn-danger {
            color: #fff;
            background-color: #930310;
            border-color: #930310;
            text-decoration: none;
        }


        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media print {
            body, html, #wrapper {
                height: 100%;
            }

            .no-print, .no-print * {
                display: none !important;
            }

            .footer {
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .content {
                /*height: 200px;*/
            }

            table {
                page-break-after: auto
            }

            tr {
                page-break-inside: avoid;
                page-break-after: auto
            }

            td {
                page-break-inside: avoid;
                page-break-after: auto
            }

            thead {
                display: table-header-group
            }

            tfoot {
                display: table-footer-group
            }

            .striped {
                border-collapse: collapse;
            }

            .striped td, .striped th {
                border: 1px solid #000;
            }

            .striped tr:nth-child(even) {
                background-color: #f4f4f4;
            }

            .striped thead th {
                font-size: 8pt;
                background: lightgray;
                font-weight: normal;
            }

            .striped tbody td {
                font-size: 8pt;
            }
        }

        /*@page {*/
        /*    sheet-size: Legal;*/
        /*    margin-top: 5mm;*/
        /*}*/

        body {
            font-family: Calibri, 'Trebuchet MS', sans-serif;
            font-weight: normal;
            font-size: 10pt;
        }

        h3,
        h4,
        h5 {
            font-weight: normal;
            margin: 0;
        }

        .striped {
            border-collapse: collapse;
        }

        .striped td, .striped th {
            border: 1px solid #000;
        }

        .striped tr:nth-child(even) {
            background-color: #f4f4f4;
        }

        .striped thead th {
            font-size: 8pt;
            background: lightgray;
            font-weight: normal;
        }

        .striped tbody td {
            font-size: 8pt;
        }

        div.bordered {
            border: 1px solid black;
            width: 50px;
            height: 20px;
            margin: auto;
        }
        .normal {
            font-weight: normal;
        }

        .bold {
            font-weight: bold;
        }
        .bold-italic {
            font-weight: bold;
            font-style: italic;
        }
        .italic {
            font-style: italic;
        }
        .underline {
            text-decoration: underline;
        }
        .noborder {
            /*border-top-color: transparent !important;*/
        }
        .nobottom {
            /*border-bottom-color: transparent !important;*/
        }
    </style>
    <style type="text/css" media="print">
        @page { size: landscape; }
    </style>
</head>
<body>
<button onclick="window.print()" class="no-print btn btn-primary">Generate PDF</button>
<a class="no-print btn btn-secondary" href="{{route('anggaran.detail.excel',[base64_encode($header->id)])}}">Generate Excel</a>
@if($header->tor != '0')
    <a class="no-print btn btn-danger" href="{{route('anggaran.download',['TOR',encrypt($header->id)])}}">Download TOR</a>
    @endif
@if($header->rab != '0')
<a class="no-print btn btn-danger" href="{{route('anggaran.download',['RAB',encrypt($header->id)])}}">Download RAB</a>
@endif
<div class="content">
    <table width="100%">
        <tr>
            <td align="center">
{{--                <img src="/app-assets/images/bg-logo.jpeg" style="height: 55px;width:140px;vertical-align: top;">--}}
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td align="center">
                <h3><b>Rencana Kerja Penganggaran</b></h3>
                <h3><b>Universitas Negeri Makassar</b></h3>
                <h3><b>Tahun {{ $header->tahun }}</b></h3>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="50%">
                <table>
                    <tr>
                        <td>Unit Kerja</td>
                        <td>:</td>
                        <td>{{$header->kode_subdit}} - {{ $header->nama }}</td>
                    </tr>
{{--                    <tr>--}}
{{--                        <td>Tahun</td>--}}
{{--                        <td>:</td>--}}
{{--                        <td>{{ $header->tahun }}</td>--}}
{{--                    </tr>--}}
                </table>
            </td>
        </tr>
    </table>
    <br>
    <div class="table">
        <table class="striped" width="100%">
            <thead>
            <tr>
{{--                <th align="center" >#</th>--}}
                <th align="center" colspan="3">Kode</th>
                <th align="left" >Program</th>
                <th align="center" colspan="7">Volume</th>
                <th align="center" >Satuan</th>
                <th align="center" >Jumlah</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td width="30px"></td>
                <td width="20px"></td>
                <td width="10px"></td>
                <td width="30px"></td>
                <td width="20px"></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" class="bold-italic">{{number_format($data->sum('jumlah'))}}</td>
            </tr>
            @foreach($gl as $index => $g)
                @php
                $kd = explode('.',$g->kode);
                $size = count($kd);
                $rowspan = 1;
                 $child = $data->where('kode_gl',$g->kode);
                    $rowspan += sizeof($child);
                //if($g->flag == '0'){
                 //   $child = $data->where('kode_gl',$g->kode);
                 //   $rowspan += sizeof($child);
                //}else{
                 //   $child = [];
                //}
                @endphp
                <tr>
{{--                    <td align="center">{{$index + 1}}</td>--}}
                    <td align="left" width="30px" class="bold" height="20px" rowspan="{{$rowspan}}">{{$size == '1' ? $kd[0]:''}}</td>
                    <td align="left" width="30px" class="bold" rowspan="{{$rowspan}}">{{$size == '2' ? $kd[1]:''}}</td>
                    <td align="center" width="50px" class="{!! $g->flag == '0' ? 'underline':'bold' !!}" rowspan="{{$rowspan}}"> {{$size > '2' ? $kd[$size-1]:''}}</td>
                    <td align="left" class="{!! $g->flag == '0' ? 'nobottom':'bold' !!}" >{{$g->nama}} ({{$g->kode}})</td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td width="20px"></td>
                    <td ></td>
{{--                    <td align="right" class="bold"> {{$g->flag != '0'? number_format($child->sum('jumlah')):''}}</td>--}}
                    <td align="right" class="{{$g->flag == '1'?'bold-italic':'italic'}}"> {{number_format($data->filter(function ($v,$k)use ($g){
    return str_contains($v->kode_gl,$g->kode);
})->sum('jumlah'))}}</td>
                </tr>
                @foreach($child as $c)
                    <tr>
{{--                        <td></td>--}}
{{--                        <td></td>--}}
{{--                        <td></td>--}}
                        <td class="noborder">-&nbsp;{{$c->kegiatan}}</td>
                        <td align="right">{{$c->var1}}</td>
                        <td align="center">{{$c->satuan_var1}}</td>
                        <td align="center">X</td>
                        <td align="right">{{$c->var2}} {{$c->satuan_var2}}</td>
                        <td align="right">{{$c->satuan_var3 != null ? $c->var3.' '.$c->satuan_var3.' X ':''}}{{$c->satuan_var4 != null ? $c->var4.' '. $c->satuan_var4:''}}</td>
                        <td align="right"> {{$c->var1 * $c->var2* $c->var3* $c->var4}}</td>
                        <td align="center">{{$c->satuan}}</td>
                        <td align="right">{{number_format($c->harga)}}</td>
                        <td align="right">{{number_format($c->harga * $c->var1 * $c->var2 * $c->var3 * $c->var4)}}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
    <br/>
    <br/>
    @if($qr != null)
        <div>
            <table width="100%">
                <tr>
                    <td width="60%" rowspan="6">
                        <img src="data:image/png;base64, {{$qr}} ">
                    </td>
                    <td>Makassar, {{date('d/m/Y')}}</td>
                </tr>
                <tr>
                    {{--                <td></td>--}}
                    <td>{{$header->jabatan}}</td>
                </tr>
                <tr>
                    {{--                <td></td>--}}
                    <td>{{$header->nama}}</td>
                </tr>
                <tr>
                    {{--                <td ></td>--}}
                    <td height="50px" ></td>
                </tr>
                <tr>
                    {{--                <td></td>--}}
                    <td><b>{{$header->leader}}</b></td>
                </tr>
                <tr>
                    {{--                <td></td>--}}
                    <td><b>NIP {{$header->nip}}</b></td>
                </tr>
            </table>
        </div>
        @endif
</div>
<br>
<div>
{{--    <img src="/app-assets/images/bg-footer-laporan-lenscape.png" style="height: 30px; width: 100%">--}}
</div>

<script>
</script>
</body>
</html>
