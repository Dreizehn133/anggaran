@extends('config.layout')
@section('menu_title',$data->nama)
@section('path1','Detail')
@section('path2',$data->tahun)
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">Pagu : Rp. {{number_format($pagu != null ? $pagu->jumlah :0)}}</h6>
                            <p class="text-muted card-sub-title">Limit : <span id="limit"></span> </p>
                        </div>
                       @if($data->flag == 0)
{{--                        <div class="pull-right">--}}
{{--                            <button class="btn btn-outline-secondary btn-block" data-effect="effect-fall"><i class="fa fa-upload"></i> &nbsp;TOR</button>&nbsp;--}}
{{--                            <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-fall" data-toggle="modal" href="#mymodal">Add New</a>--}}
{{--                        </div>--}}
                            <div class="pull-right">
                                <div class="justify-content-center">
                                    <button type="button" class="btn btn-primary my-2 mr-2" onclick="upload('tor')">
                                        <i class="fa fa-upload mr-2"></i> TOR
                                    </button>
                                    <button type="button" class="btn btn-success my-2 mr-2" onclick="upload('rab')">
                                        <i class="fa fa-file-upload mr-2"></i> RAB
                                    </button>
                                    <input type="file" style="display: none" id="file" name="file" accept="application/pdf"/>
                                    <input type="file" style="display: none" id="file2" name="file2" accept="application/pdf"/>
                                    <a class="modal-effect btn btn-outline-primary  my-2 btn-icon-text" data-effect="effect-fall" data-toggle="modal" href="#mymodal"><i class="fa fa-plus-circle"></i> Add New</a>
                                </div>
                            </div>
                           @endif
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Program</th>
                                <th>Kegiatan</th>
                                <th>Var 1</th>
                                <th>Var 2</th>
                                <th>Var 3</th>
                                <th>Var 4</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Berkas</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
                @if($data->flag == '0')
                    <div class="card-footer">
                        <div class="tx-center">
                            <button class="btn btn-primary" onclick="changeStatus()">Finish</button>
                        </div>
                    </div>
                    @endif
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Detail Anggaran</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                    <div class="modal-body">
                        <input type="hidden" id="ids" name="ids"/>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" id="gl1-group">
                                    <label>Program Lv1 </label>
                                    <select class="form-control select2" id="gl1" name="gl1">
                                        <option value="">Select Lv 1</option>
                                        @foreach ($gl as $g)
                                            <option value="{{$g->kode}}">{{$g->kode}} - {{$g->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="gl2-group">
                                    <label>Program Lv2 </label>
                                    <select class="form-control select2" id="gl2" name="gl2">
                                        <option value="">Select Lv 2</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="gl3-group">
                                    <label>Program Lv3 </label>
                                    <select class="form-control select2" id="gl3" name="gl3">
                                        <option value="">Select Lv 3</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" id="gl4-group">
                                    <label>Program Lv4 </label>
                                    <select class="form-control select2" id="gl4" name="gl4">
                                        <option value="">Select Lv 4</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="gl5-group">
                                    <label>Program Lv5 </label>
                                    <select class="form-control select2" id="gl5" name="gl5">
                                        <option value="">Select Lv 5</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4" id="gl6-container">
                                <div class="form-group" id="gl6-group">
                                    <label>Program Lv6 </label>
                                    <select class="form-control select2" id="gl6" name="gl6">
                                        <option value="">Select Lv 6</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="kegiatan-group">
                            <label>Kegiatan</label>
                            <input type="text" class="form-control" name="kegiatan" id="kegiatan" required/>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="var1-group">
                                <label>Variable 1</label>
                               <input type="number" class="form-control" name="var1" id="var1" min="1" required/>
                            </div>
                            <div class="col-md-6" id="svar1-group">
                                <label>Satuan Var. 1</label>
                                <input type="text" class="form-control" name="svar1" id="svar1"/>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6" id="var2-group">
                                <label>Variable 2</label>
                                <input type="number" class="form-control" name="var2" id="var2" min="1"/>
                            </div>
                            <div class="col-md-6" id="svar2-group">
                                <label>Satuan Var. 2</label>
                                <input type="text" class="form-control" name="svar2" id="svar2"/>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6" id="var3-group">
                                <label>Variable 3</label>
                                <input type="number" class="form-control" name="var3" id="var3" min="1"/>
                            </div>
                            <div class="col-md-6" id="svar3-group">
                                <label>Satuan Var. 3</label>
                                <input type="text" class="form-control" name="svar3" id="svar3"/>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6" id="var4-group">
                                <label>Variable 4</label>
                                <input type="number" class="form-control" name="var4" id="var4" min="1"/>
                            </div>
                            <div class="col-md-6" id="svar4-group">
                                <label>Satuan Var. 4</label>
                                <input type="text" class="form-control" name="svar4" id="svar4"/>
                            </div>
                        </div>
                        <br/>
{{--                        <div class="form-group" id="detail-group">--}}
{{--                            <label>Detail</label>--}}
{{--                            <input type="text" class="form-control" name="detail" id="detail"/>--}}
{{--                        </div>--}}
{{--                        <div class="form-group" id="qty-group">--}}
{{--                            <label>Qty</label>--}}
{{--                            <input type="number" min="1" class="form-control" name="qty" id="qty" required/>--}}
{{--                        </div>--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="jumlah-group">
                                    <label>Harga</label>
                                    <input type="number" class="form-control" name="jumlah" id="jumlah" min="1" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="satuan-group">
                                    <label>Satuan</label>
                                    <input type="text" class="form-control" name="satuan" id="satuan"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Dikelolah Oleh</label>
                                <div class="col-md-6">
                                    <label class="ckbox"><input type="checkbox" name="kp"><span>KANTOR PUSAT</span></label>
                                </div>
                                <br/>
                                <div class="col-md-6 mg-t-20 mg-lg-t-0">
                                    <label class="ckbox"><input type="checkbox" name="it"><span>CLEREANCE/IT</span></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label>IKU</label>
                                    <select class="select2 form-control" name="iku">
                                        @foreach($pengelola as $p)
                                            <option value="{{$p->code}}">{{$p->code}} - {{$p->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
{{--                        <div class="form-group" id="ket-group" @if(\Illuminate\Support\Facades\Auth::user()->role != env('ROLE_ADMIN')){!! 'hidden' !!}@endif>--}}
{{--                            <label>Catatan</label>--}}
{{--                           <textarea id="ket" name="ket" class="form-control"></textarea>--}}
{{--                        </div>--}}
                    </div>
                    <div class="modal-footer">
                        <button class="btn ripple btn-primary" type="submit" >Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal2">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Catatan</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form2" method="POST">
                    <div class="modal-body">
                        <input type="hidden" id="idh" name="idh"/>
                        <div class="form-group" id="ket-group">
                            <label>Catatan</label>
                            <textarea id="cttn" name="cttn" class="form-control"></textarea>
                        </div>
                        <div class="main-chat-body ps ps--active-y" id="ChatBody">
                            <div class="content-inner" id="catatanlog">
{{--                                <div class="media flex-row-reverse">--}}
{{--                                    <div class="main-img-user online"><img alt="avatar" src="/assets/img/1.jpg"></div>--}}
{{--                                    <div class="media-body">--}}
{{--                                        <div class="main-msg-wrapper">--}}
{{--                                            Nulla consequat massa quis enim. Donec pede justo, fringilla vel...--}}
{{--                                        </div>--}}
{{--                                        <div>--}}
{{--                                            <span>9:48 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="media">--}}
{{--                                    <div class="main-img-user online"><img alt="avatar" src="/assets/img/1.jpg"></div>--}}
{{--                                    <div class="media-body">--}}
{{--                                        <div class="main-msg-wrapper">--}}
{{--                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.--}}
{{--                                        </div>--}}
{{--                                        <div>--}}
{{--                                            <span>9:32 am</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <div class="ps__rail-x" style="left: 0px; top: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 714px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 512px;"></div></div></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn ripple btn-primary" type="submit" >Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('internal_js')
    <script>
        var table;
        var pagu = '{{$pagu->jumlah??0}}'

        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            $('#gl6-container').hide()
            $('#tahun').change(function (e){
                table.ajax.reload();
            });
            getTotal()
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('anggaran.detail.data')}}',
                    data: function (e) {
                        e.anggaran = '{{$data->anggaran}}'
                    }
                },
                drawCallback: function( settings ) {
                    var api = this.api();

                    // Output the data for the visible rows to the browser's console
                    // console.log( api.rows( {page:'current'} ).data() );
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'kode_gl',name :'anggaran_detail.kode_gl'},
                    {data: 'kegiatan',name :'anggaran_detail.kegiatan'},
                    // {data: 'detail',name :'anggaran_detail.detail'},
                    // {data: 'qty',name :'anggaran_detail.qty'},
                    {data: 'var1',name :'anggaran_detail.var1'},
                    {data: 'var2',name :'anggaran_detail.var2'},
                    {data: 'var3',name :'anggaran_detail.var3'},
                    {data: 'var4',name :'anggaran_detail.var4'},
                    {data: 'harga',name :'anggaran_detail.harga'},
                    {data: 'jumlah',searchable: false},
                    {data: 'files',searchable: false},
                    // {data: 'ket',name :'anggaran_detail.ket'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                search: {
                    "regex": true
                },
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })
        $('#form').submit(function (even) {
            even.preventDefault();
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            if($('#gl6-container').is(':visible') && $('#gl6').val() == ''){
                $('#gl6-group').addClass('has-error');
                $('#gl6-group').append('<span class="help-block">Pilih Program!</span>');
                return;
            }else if($('#gl5').val() == ''){
                $('#gl5-group').addClass('has-error');
                $('#gl5-group').append('<span class="help-block">Pilih Program!</span>');
                return;
            }
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            formData.append('anggaran', '{{$data->anggaran}}')
            formData.append('gl', $('#gl6-container').is(':visible') ? $('#gl6').val() : $('#gl5').val())
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
                {
                    if(!val){
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('anggaran.detail.post')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            getTotal()
                            $('#mymodal').modal('hide')
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            table.ajax.reload()
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        $('#form2').submit(function (even) {
            even.preventDefault();
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
                {
                    if(!val){
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('anggaran.catatan.post')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            // $('#mymodal2').modal('hide')
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            getCatatan(formData.get('idh'));
                            // table.ajax.reload()
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        function getCatatan(id){
            $('#catatanlog').empty()
            $.ajax({
                type: 'GET',
                url: '{{route('anggaran.detail.data')}}/get_catatan/' + id,
                dataType: 'json',
                success: function (data) {
                    $.each(data, function(key, value) {
                        let clss = 'media';
                        if(key%2==0)clss += ' flex-row-reverse';
                        $('#catatanlog')
                            .append('<div class="media '+clss+'"> <div class="main-img-user online"><img alt="avatar" src="/assets/img/1.jpg"></div> <div class="media-body"> <div class="main-msg-wrapper">'+value.catatan+' </div> <div> <span>'+value.created_at+"\t oleh : "+value.nama+'</span> <a href=""><i class="icon ion-android-more-horizontal"></i></a></div></div></div>');
                    });
                }
            })
        }
        function reset(){
            $('#form')[0].reset()
            $('.select2').val($(".select2 option:first").val()).trigger('change')
            $('#ids').val('')
            kd = null;
        }
        var kd = null;
        function edit(id){
            $('#ids').val(id)
            $.ajax({
                type: 'GET',
                url: '{{route('anggaran.detail.data')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    // $('#gl').val(data.kode_gl).trigger('change')
                    $('#kegiatan').val(data.kegiatan)
                    $('#var1').val(data.var1)
                    $('#var2').val(data.var2)
                    $('#svar1').val(data.satuan_var1)
                    $('#svar2').val(data.satuan_var2)
                    $('#jumlah').val(data.harga)
                    $('#satuan').val(data.satuan)
                    $('#ket').val(data.ket)
                    kd = data.kode_gl.split('.')
                    $('#gl1').val(kd[0]).trigger('change')
                    $('#mymodal').modal()
                }
            })
        }
        function getTotal(){
            $.ajax({
                type: 'GET',
                url: '{{route('anggaran.data')}}/' + '{{$data->anggaran}}',
                dataType: 'json',
                success: function (data) {
                    $('#limit').text(numberWithCommas(pagu - data));
                    // console.log(data)
                }
            })
        }
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        function remove(id){
            swal({
                title: "Are you sure?",
                text: "You will Remove Detail!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('anggaran.detail.remove')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    processData:false,
                    dataType: 'json',
                    success: function (data) {
                        getTotal()
                        // toastr.success(data, 'Success', {timeOut: 5000});
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        function changeStatus(){
            swal({
                title: "Are you sure?",
                text: "You will Finish?",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Finish!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('anggaran.change')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': '{{$data->anggaran}}',
                        'status': '1'
                    },
                    dataType: 'json',
                    success: function (data) {
                        window.location = '{{route('anggaran.index')}}'
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        console.log(json);
                        toastr.error(json, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        $('#mymodal').on('hidden.bs.modal', function (e) {
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            $('#gl1').val("").trigger('change')
            $('#gl6-container').hide()
            reset();// remove the error text
        });
        $('#gl1, #gl2, #gl3, #gl4, #gl5').change(function (){
            let id = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '{{ route('anggaran.gl')}}?ids='+$(this).val(),
                success: function (data) {
                    if(id == 'gl1'){
                        $('#gl2').find('option').not(':first').remove();
                        $('#gl3').find('option').not(':first').remove();
                        $('#gl4').find('option').not(':first').remove();
                        $('#gl5').find('option').not(':first').remove();
                        $('#gl6').find('option').not(':first').remove();
                        $.each(data.data, function(key, value) {
                            $('#gl2')
                                .append($("<option></option>")
                                    .attr("value", value.kode)
                                    .text(value.kode +' - '+value.nama));
                        });
                        if(kd != null){
                            $('#gl2').val(kd[0]+'.'+kd[1]).trigger('change')
                        }
                    }else if(id =='gl2'){
                        $('#gl3').find('option').not(':first').remove();
                        $('#gl4').find('option').not(':first').remove();
                        $('#gl5').find('option').not(':first').remove();
                        $('#gl6').find('option').not(':first').remove();
                        $.each(data.data, function(key, value) {
                            $('#gl3')
                                .append($("<option></option>")
                                    .attr("value", value.kode)
                                    .text(value.kode +' - '+value.nama));
                        });
                        if(kd != null){
                            $('#gl3').val(kd[0]+'.'+kd[1]+'.'+kd[2]).trigger('change')
                        }
                    }else if(id =='gl3'){
                        $('#gl4').find('option').not(':first').remove();
                        $('#gl5').find('option').not(':first').remove();
                        $('#gl6').find('option').not(':first').remove();
                        $.each(data.data, function(key, value) {
                            $('#gl4')
                                .append($("<option></option>")
                                    .attr("value", value.kode)
                                    .text(value.kode +' - '+value.nama));
                        });
                        if(kd != null){
                            $('#gl4').val(kd[0]+'.'+kd[1]+'.'+kd[2]+'.'+kd[3]).trigger('change')
                        }
                    }else if(id =='gl4'){
                        $('#gl5').find('option').not(':first').remove();
                        $('#gl6').find('option').not(':first').remove();
                        $.each(data.data, function(key, value) {
                            $('#gl5')
                                .append($("<option></option>")
                                    .attr("value", value.kode)
                                    .text(value.kode +' - '+value.nama));
                        });
                        if(kd != null){
                            $('#gl5').val(kd[0]+'.'+kd[1]+'.'+kd[2]+'.'+kd[3]+'.'+kd[4]).trigger('change')
                        }
                        if(data.data.length > 0){
                            if(data.data[0].flag == "1"){
                                $('#gl6-container').show()
                            }else{
                                $('#gl6-container').hide()
                            }
                        }
                    }else if(id =='gl5'){
                        $('#gl6').find('option').not(':first').remove();
                        if(data.data.length > 0){
                            $.each(data.data, function(key, value) {
                                $('#gl6')
                                    .append($("<option></option>")
                                        .attr("value", value.kode)
                                        .text(value.kode +' - '+value.nama));
                            });
                        }
                        if(kd != null && kd.length > 5){
                            $('#gl6').val(kd[0]+'.'+kd[1]+'.'+kd[2]+'.'+kd[3]+'.'+kd[4]+'.'+kd[5]).trigger('change')
                        }
                    }

                },
                error: function (request, status, error) {

                }
            })
        })
        function editCatatan(id){
            getCatatan(id)
            $('#idh').val(id)
            $.ajax({
                type: 'GET',
                url: '{{route('anggaran.detail.data')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    $('#cttn').val(data.ket)
                    $('#mymodal2').modal()
                }
            })
        }
        function upload(type){
            if(type == 'tor'){
                $('#file').attr('accept','.pdf, .docx')
            }else{
                $('#file').attr('accept','.pdf, .xlsx')
            }
            types = type;
            $('#file').click()
        }
        var types = null;
        $('#file').change(function () {
            if($(this).prop('files')[0] == undefined){
                return;
            }
            console.log('trigger upload')
            console.log(types)
            var formData = new FormData();
            formData.append('_token','{{csrf_token()}}')
            formData.append('type',types)
            formData.append('ids','{{base64_encode($data->id)}}')
            formData.append('file',$(this).prop('files')[0])
            $.ajax({
                type: 'POST',
                url: '{{route('anggaran.upload')}}',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    toastr.success(data, 'Success', {timeOut: 5000});
                },
                error: function (request, status, error) {
                    //json = $.parseJSON(request.responseText);
                    toastr.error(request.responseText, error, {timeOut: 5000});
                }
            });
        })

        function uploadDetail(type, id){
            if(type == 'KAK'){
                $('#file2').attr('accept','.pdf, .docx')
            }else{
                $('#file2').attr('accept','.pdf, .xlsx')
            }
            typesDt = type;
            idsx = id
            $('#file2').click()
        }
        var typesDt = null;
        var idsx = null;
        $('#file2').change(function () {
            if($(this).prop('files')[0] == undefined){
                return;
            }
            var formData = new FormData();
            formData.append('_token','{{csrf_token()}}')
            formData.append('type',typesDt)
            formData.append('ids',idsx)
            formData.append('file',$(this).prop('files')[0])
            $.ajax({
                type: 'POST',
                url: '{{route('anggaran.upload.detail')}}',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    table.ajax.reload()
                    toastr.success(data, 'Success', {timeOut: 5000});
                },
                error: function (request, status, error) {
                    //json = $.parseJSON(request.responseText);
                    toastr.error(request.responseText, error, {timeOut: 5000});
                }
            });
        })
    </script>
@endsection
