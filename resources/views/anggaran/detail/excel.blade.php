<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        .normal {
            font-weight: normal;
        }

        .bold {
            font-weight: bold;
        }
        .underline {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th colspan="13" align="center"><b>Rencana Kerja Penganggaran</b></th>
    </tr>
    <tr>
        <th colspan="13" align="center"><b>Universitas Negeri Makassar</b></th>
    </tr>
    <tr>
        <th colspan="13" align="center"><b>Tahun {{ $header->tahun }}</b></th>
    </tr>
</table>
<table>
    <tr>
        <td colspan="4">Unit Kerja : {{$header->kode_subdit}} - {{ $header->nama }}</td>
    </tr>
</table>
<table class="striped" width="100%" border="1">
    <thead>
    <tr>
        {{--                <th align="center" >#</th>--}}
        <th align="center" colspan="3"><b>Kode</b></th>
        <th align="center" ><b>Program</b></th>
        <th align="center" colspan="7"><b>Volume</b></th>
        <th align="center" ><b>Satuan</b></th>
        <th align="center" ><b>Jumlah</b></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td width="30px"></td>
        <td width="20px"></td>
        <td width="10px"></td>
        <td width="30px"></td>
        <td width="20px"></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="right" class="bold"><b>{{number_format($data->sum('jumlah'))}}</b></td>
    </tr>
    @foreach($gl as $index => $g)
        @php
            $kd = explode('.',$g->kode);
            $size = count($kd);
            $rowspan = 1;
            if($g->flag == '0'){
                $child = $data->where('kode_gl',$g->kode);
                $rowspan += sizeof($child);
            }else{
                $child = [];
            }
        @endphp
        <tr>
            {{--                    <td align="center">{{$index + 1}}</td>--}}
            <td align="left" width="30px" class="bold" height="20px" rowspan="{{$rowspan}}"><b>{{$size == '1' ? $kd[0]:''}}</b></td>
            <td align="left" width="30px" class="bold" rowspan="{{$rowspan}}"><b>{{$size == '2' ? $kd[1]:''}}</b></td>
            <td align="center" width="50px" rowspan="{{$rowspan}}"> {!! $g->flag == '0' ? '':'<b>' !!}{{$size > '2' ? $kd[$size-1]:''}}{!! $g->flag == '0' ? '':'</b>' !!}</td>
            <td align="left" class="{!! $g->flag == '0' ? 'nobottom':'bold' !!}" >{!! $g->flag == '0' ? '<u>':'<b>' !!}{{$g->nama}}{!! $g->flag == '0' ? '</u>':'</b>' !!}</td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td width="20px"></td>
            <td ></td>
            <td align="right" class="bold"><b>{{$g->flag == '0'? number_format($child->sum('jumlah')):''}}</b></td>
        </tr>
        @foreach($child as $c)
            <tr>
                {{--                        <td></td>--}}
                {{--                        <td></td>--}}
                {{--                        <td></td>--}}
                <td class="noborder">-&nbsp;{{$c->kegiatan}}</td>
                <td align="right">{{$c->var1}}</td>
                <td align="center">{{$c->satuan_var1}}</td>
                <td align="center">X</td>
                <td align="right">{{$c->var2}} {{$c->satuan_var2}}</td>
                <td align="right">{{$c->satuan_var3 != null ? $c->var3.' '.$c->satuan_var3.' X ':''}}{{$c->satuan_var4 != null ? $c->var4.' '. $c->satuan_var4:''}}</td>
                <td align="right"> {{$c->var1 * $c->var2 * $c->var3 * $c->var4}}</td>
                <td align="center">{{$c->satuan}}</td>
                <td align="right">{{number_format($c->harga)}}</td>
                <td align="right">{{number_format($c->harga * $c->var1 * $c->var2 * $c->var3 * $c->var4)}}</td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
<table width="100%">
    <tr>
        <td colspan="11"></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="11"></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td width="76%" colspan="11" rowspan="8">
{{--            <img src="data:image/png;base64, {{$qr}} ">--}}
        </td>
        <td colspan="2">Makassar, {{date('d/m/Y')}}</td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2">{{$header->jabatan}}</td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2">{{$header->nama}}</td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2"></td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2"></td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2"></td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2"><b>{{$header->leader}}</b></td>
    </tr>
    <tr>
{{--        <td colspan="11"></td>--}}
        <td colspan="2"><b>NIP {{$header->nip}}</b></td>
    </tr>
</table>

</body>
</html>
