@foreach($data as $d)
    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="card custom-card">
            <div class="card-body">
                <div class="card-item">
                    <div class="card-item-icon card-icon">
                        {{--                                <svg class="text-primary" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><g><rect height="14" opacity=".3" width="14" x="5" y="5"></rect><g><rect fill="none" height="24" width="24"></rect><g><path d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z"></path><rect height="5" width="2" x="7" y="12"></rect><rect height="10" width="2" x="15" y="7"></rect><rect height="3" width="2" x="11" y="14"></rect><rect height="2" width="2" x="11" y="10"></rect></g></g></g></svg>--}}
                        <a href="{{route('anggaran.detail.preview',[base64_encode($d->id)])}}"
                           target="_blank"><i class="fa fa-eye"></i></a>
                    </div>
                    <div class="card-item-title mb-2">
                        <label class="main-content-label tx-13 font-weight-bold mb-1">{{$d->tahun}}</label>
                        <span class="d-block tx-12 mb-0 text-muted">{{$d->nama}}</span>
                    </div>
                    <div class="card-item-body">
                        <div class="card-item-stat">
                            <h4 class="font-weight-bold">{{number_format(\App\Models\AnggaranDetail::where('id_anggaran',$d->id)->where('active',true)->first(\Illuminate\Support\Facades\DB::raw('SUM(harga*var1*var2*var3*var4) as total'))->total)}}</h4>
                            <h5 class="font-weight-bold">Limit : {{number_format($d->pagu)}}</h5>
                            <small><b class="text-success"></b> {{$d->user}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
