@extends('config.layout')
@section('menu_title','RKA Report')
@section('path1','Transaction')
@section('path2','Anggaran')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">Laporan RKA</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                            <div class="form-group">
                                <select class="form-control select2" id="tahun">
                                    @foreach($years as $y)
                                        <option value="{{$y}}" {!! $y == date('Y') ? 'selected' : '' !!}>{{$y}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Unit</th>
                                <th>Nama Unit</th>
                                <th>Kode Program</th>
                                <th>Kegiatan</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Berkas</th>
                                <th>Dibuat Oleh</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('internal_js')
    <script>
        var table;
        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            $('#tahun').change(function (e){
                table.ajax.reload();
            });
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('report_rka.data')}}',
                    data: function (e) {
                        e.tahun = $('#tahun').val()
                    }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'kode',name :'ms.kode'},
                    {data: 'nama',name :'ms.nama'},
                    {data: 'kode_gl',name :'anggaran_detail.kode_gl'},
                    {data: 'kegiatan',name :'anggaran_detail.kegiatan'},
                    {data: 'jumlah'},
                    {data: 'harga',name :'anggaran_detail.harga'},
                    {data: 'files'},
                    {data: 'user',name :'us.nama'},
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })


    </script>
@endsection
