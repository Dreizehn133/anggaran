<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Anggaran Apps">
    <meta name="author" content="Anggaran">
    <meta name="keywords" content="anggaran kampus">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/brand/favicon.ico')}}" type="image/x-icon"/>

    <!-- Title -->
    <title>Anggaran</title>

    <!-- Bootstrap css-->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>

    <!-- Icons css-->
    <link href="{{asset('assets/plugins/web-fonts/icons.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/web-fonts/font-awesome/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/web-fonts/plugin.css')}}" rel="stylesheet"/>

    <!-- Style css-->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/skins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/dark-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/colors/default.css')}}" rel="stylesheet">

    <!-- Color css-->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/colors/color4.css')}}">
    <style>
        .bg-primary {
            background-color: #0d3354 !important;
        }
    </style>
</head>

<body class="main-body leftmenu">

<!-- Loader -->
<div id="global-loader">
    <img src="{{asset('assets/img/loader.svg')}}" class="loader-img" alt="Loader">
</div>
<!-- End Loader -->

<!-- Page -->
<div class="page main-signin-wrapper">

    <!-- Row -->
    <div class="row signpages text-center">
        <div class="col-md-12">
            <div class="card">
                <div class="row row-sm">
                    <div class="col-lg-6 col-xl-5 d-none d-lg-block text-center bg-primary details align-items-center">
                        <div class="p-2 align-items-center pos-relative" style="height: 100%">
{{--                            <img src="{{asset('assets/img/logos/logo-only.png')}}" class="header-brand-img text-center float-center ht-50" alt="logo">--}}
{{--                            <div class="clearfix"></div>--}}
                            <img src="{{asset('assets/img/logos/rekap.png')}}" class="mt-5" alt="user" style="object-fit: fill">
{{--                            <h5 class="mt-4 text-white">Login With Your Registered Account</h5>--}}
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-7 col-xs-12 col-sm-12 login_form ">
                        <div class="container-fluid">
                            <div class="row row-sm">
                                <div class="card-body mt-2 mb-2 ml-0">
                                    <img src="{{asset('assets/img/logos/logo-only.png')}}" class=" d-lg-none header-brand-img text-center float-center ht-100 mb-10" alt="logo">
                                    <div class="clearfix"></div>
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <h5 class="text-left mb-2">Signin to Your Account</h5>
                                        <p class="mb-4 text-muted tx-13 ml-0 text-left">Silahkan melakukan Login menggunakan username dan password yang telah anda dapatkan dari Admin Anggaran</p>
                                        <div class="form-group text-left">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="Enter your email" type="text" name="email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group text-left">
                                            <label>Password</label>
                                            <input id="password" placeholder="Enter your password"  type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror                                        </div>
                                        <button class="btn ripple btn-main-primary btn-block">Sign In</button>
                                    </form>
                                    <div class="text-left mt-5 ml-0">
                                        <div class="mb-1"><a href="">Forgot password?</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->

</div>
<!-- End Page -->

<!-- Jquery js-->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{asset('assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Select2 js-->
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- Custom js -->
<script src="{{asset('assets/js/custom.js')}}"></script>

</body>
</html>
