@extends('config.layout')
@section('menu_title','RPD')
@section('path1','Anggaran')
@section('path2','RPD')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">RPD</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                            <div class="form-inline">
                                <div class="form-group">
                                    <select class="form-control select2" id="tahun" width="30%">
                                        @foreach($years as $y)
                                            <option value="{{$y}}" {!! $y == date('Y') ? 'selected' : '' !!}>{{$y}}&nbsp;&nbsp;</option>
                                        @endforeach
                                    </select>
                                </div>
                                &nbsp;
                                <div class="form-group">
                                    <select class="form-control select2" id="subdit" width="50%">
                                        @if(Auth::user()->get_role_name->level == '1')
                                            <option value="" selected>* Semua&nbsp;&nbsp;</option>
                                        @endif
                                        @foreach($subdit as $y)
                                            <option value="{{$y->kode}}">{{$y->kode}} - {{$y->nama}}&nbsp;&nbsp;</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Unit</th>
                                <th>Kode Program</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>RPD</th>
                                <th>Berkas RPD</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">RPD</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                    <div class="modal-body">
                        <input type="hidden" id="ids" name="ids"/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input type="number" class="form-control" id="jumlah" name="jumlah" min="0" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" class="form-control" id="harga" name="harga" min="0" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-lg">
                                    <table id="myDataTable2" class="table table-bordered border-t0 key-buttons text-nowrap" style="width: 100%!important;">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                            <th>Total</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning my-2 mr-2" onclick="upload('rpd')">
                            <i class="fa fa-upload mr-2"></i> RPD Kegiatan
                        </button>
                        <input type="file" style="display: none" id="file" name="file" accept="application/pdf"/>
                        <button class="btn ripple btn-primary" type="submit" >Save</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('internal_js')
    <script>
        var table;
        var table2;
        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            $('#tahun').change(function (e){
                table.ajax.reload();
            });
            $('#subdit').change(function (e){
                table.ajax.reload();
            });
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('rpd.data')}}',
                    data: function (e) {
                        e.tahun = $('#tahun').val()
                        e.subdit = $('#subdit').val()
                    }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'nama',name :'ms.nama'},
                    {data: 'kode_gl',name :'anggaran_detail.kode_gl'},
                    {data: 'kegiatan',name :'anggaran_detail.kegiatan'},
                    {data: 'jumlah'},
                    {data: 'harga',name :'anggaran_detail.harga'},
                    {data: 'total'},
                    {data: 'total_rpd'},
                    {data: 'files'},
                    {data: 'action'},
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });

            table2 = $('#myDataTable2').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('rpd.detail.data')}}',
                    data: function (e) {
                        e.ids = $('#ids').val()
                    }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'jumlah',name:'anggaran_rpd.jumlah'},
                    {data: 'harga',name:'anggaran_rpd.harga'},
                    {data: 'total'},
                    {data: 'keterangan',name:'anggaran_rpd.keterangan'},
                    {data: 'action'},
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })

        function showRPD(id){
            $('#ids').val(id)
            $('#mymodal').modal()
            table2.ajax.reload()
        }
        function reset(){
            $('#form')[0].reset()
            $('#ids').val('')
        }
        $('#form').submit(function (even) {
            even.preventDefault();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
                {
                    if(!val){
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('rpd.post')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            // $('#mymodal').modal('hide')
                            table2.ajax.reload()
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        $('#mymodal').on('hidden.bs.modal', function (e) {
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            reset();// remove the error text
            table.ajax.reload()
        });
        function deleteRPD(id){
            swal({
                title: "Are you sure?",
                text: "You will Delete Data!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('rpd.detail.remove')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Success', {timeOut: 5000});
                        table2.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        function upload(type){
            // $('#file').attr('accept','.pdf, .docx')
            $('#file').click()
        }
        $('#file').change(function () {
            if($(this).prop('files')[0] == undefined){
                return;
            }
            var formData = new FormData();
            formData.append('_token','{{csrf_token()}}')
            formData.append('ids',$('#ids').val())
            formData.append('file',$(this).prop('files')[0])
            $.ajax({
                type: 'POST',
                url: '{{route('rpd.upload')}}',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    toastr.success(data, 'Success', {timeOut: 5000});
                },
                error: function (request, status, error) {
                    //json = $.parseJSON(request.responseText);
                    toastr.error(request.responseText, error, {timeOut: 5000});
                }
            });
        })
    </script>
@endsection
