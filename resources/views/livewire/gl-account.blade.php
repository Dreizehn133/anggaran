<div>
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">GL Account Data</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                        </div>
                        <div class="pull-right">
                            <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-fall" data-toggle="modal" href="#mymodal">Add New</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Account</th>
                                <th>Name</th>
                                <th>Flag</th>
                                <th>Add By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $index => $d)
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td>{{$d->kode}}</td>
                                        <td>{{$d->nama}}</td>
                                        <td>@if($d->flag)<span class="badge badge-pill badge-primary">Header</span>
                                            @else
                                                <span class="badge badge-pill badge-secondary">Detail</span>
                                            @endif
                                        </td>
                                        <td>{{$d->user}}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">GL Account</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <p class="mg-b-10">Kode Header</p>
                            <div wire:ignore>
                                <select name="header" class="form-control select2" wire:model="header_id" id="header_id" name="header_id">
                                    <option value="0">No Header</option>
                                    @foreach($data_header as $d)
                                        <option value="{{$d->id}}">{{$d->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="mg-b-10">Kode</p>
                            <input type="text" class="form-control" placeholder="Kode Akun" wire:model.defer="kode">
                        </div>
                        <div class="form-group">
                            <p class="mg-b-10">Nama</p>
                            <input type="text" class="form-control" placeholder="Nama Akun" wire:model.defer="nama">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn ripple btn-primary" type="button" wire:click.prevent="store">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                table = $('#myDataTable').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                } );
                table.buttons().container()
                    .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
                // $('.select2').select2({
                //     width : '100%'
                // });
                // $('.select2').on('change', function (e) {
                //     var data = $('.select2').select2("val");
                //     @this.set('ottPlatform', data);
                // });
            });
                // $('.select2').on('change', function (e) {
                //     let elementName = $(this).attr('id');
                //     var data = $(this).select2("val");
                // @this.set(elementName, data);
                // });
            // });
        </script>
    @endpush
</div>
{{--@section('modal')--}}
{{--   --}}
{{--@endsection--}}
