<ul class="nav">
    @php
        $title = \App\Models\Menu::where('title',true)
            ->join('menu_user as mu',function ($e){
                return $e->on('mu.menu_id','menu.id')
                ->where('mu.role',\Illuminate\Support\Facades\Auth::user()->role)
                ->where('mu.active',true);
            })
            ->where('menu.active',true)->with(['get_menu.get_child'])->orderBy('menu.index')->get(['menu.*']);
    @endphp
    @foreach($title as $t)
        <li class="nav-header"><span class="nav-label">{{$t->name}}</span></li>
        @php
          $menu = $t->get_menu;
        @endphp
        @foreach ($menu as $m)
            <li class="nav-item">
                @if($m->header == '0')
                    <a class="nav-link" href="{{$m->url}}"><span class="shape1"></span><span class="shape2"></span><i class="{{$m->icon}} sidemenu-icon"></i><span class="sidemenu-label">{{$m->name}}</span></a>
                @else
                    <a class="nav-link with-sub" href="#"><span class="shape1"></span><span class="shape2"></span><i class="{{$m->icon}} sidemenu-icon"></i><span class="sidemenu-label">{{$m->name}}</span><i class="angle fe fe-chevron-right"></i></a>
                    @php
                    $submenu = $m->get_child;
                    @endphp
                    <ul class="nav-sub">
                        @foreach($submenu as $s)
                            <li class="nav-sub-item"><a class="nav-sub-link" href="{{$m->url}}{{$s->url}}">{{$s->name}}</a></li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    @endforeach
    @if(\Illuminate\Support\Facades\Auth::user()->get_role_name->level == '1')
        <li class="nav-header"><span class="nav-label">Developer</span></li>
        <li class="nav-item">
            <a class="nav-link with-sub" href="#"><span class="shape1"></span><span class="shape2"></span><i class="fa fa-desktop sidemenu-icon"></i><span class="sidemenu-label">Menu</span><i class="angle fe fe-chevron-right"></i></a>
            <ul class="nav-sub">
                <li class="nav-sub-item"><a class="nav-sub-link" href="/developer/menu">Menu</a></li>
                <li class="nav-sub-item"><a class="nav-sub-link" href="/developer/role_menu">Menu Role</a></li>
            </ul>

        </li>
    @endif
</ul>
