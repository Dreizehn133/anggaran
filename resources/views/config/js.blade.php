
<!-- Jquery js-->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{asset('assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Internal Chart.Bundle js-->
<script src="{{asset('assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>

<!-- Peity js-->
<script src="{{asset('assets/plugins/peity/jquery.peity.min.js')}}"></script>

<!-- Select2 js-->
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- Perfect-scrollbar js -->
<script src="{{asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

<!-- Sidemenu js -->
<script src="{{asset('assets/plugins/sidemenu/sidemenu.js')}}"></script>

<!-- Sidebar js -->
<script src="{{asset('assets/plugins/sidebar/sidebar.js')}}"></script>

<!-- Internal Morris js -->
<script src="{{asset('assets/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('assets/plugins/morris.js/morris.min.js')}}"></script>

<!-- Circle Progress js-->
{{--<script src="/assets/js/circle-progress.min.js"></script>--}}
{{--<script src="/assets/js/chart-circle.js"></script>--}}

<!-- Internal Dashboard js-->
{{--<script src="/assets/js/index.js"></script>--}}

<!-- Sticky js -->
<script src="{{asset('assets/js/sticky.js')}}"></script>

<!-- Custom js -->
<script src="{{asset('assets/js/custom.js')}}"></script>

<!-- Select2 js-->
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- Internal Data Table js -->
<script src="{{asset('assets/plugins/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/fileexport/buttons.colVis.min.js')}}"></script>
{{--<script src="/assets/js/table-data.js"></script>--}}

<!-- Perfect-scrollbar js -->
<script src="{{asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('assets/js/modal.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    $(function (){
        $("input[required], select[required]").attr("oninvalid", "this.setCustomValidity('Required!')");
        $("input[required], select[required]").attr("oninput", "setCustomValidity('')");
    });
</script>
