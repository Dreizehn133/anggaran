<!-- Bootstrap css-->
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>

<!-- Icons css-->
<link href="{{asset('assets/plugins/web-fonts/icons.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/plugins/web-fonts/font-awesome/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/web-fonts/plugin.css')}}" rel="stylesheet"/>

<!-- Style css-->
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/skins.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/dark-style.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/colors/default.css')}}" rel="stylesheet">

<!-- Color css-->
<link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/css/colors/color4.css')}}">

<!-- Select2 css-->
<link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<!-- Mutipleselect css-->
<link rel="stylesheet" href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}">

<!-- Sidemenu css-->
<link href="{{asset('assets/css/sidemenu/sidemenu-closed.css')}}" rel="stylesheet">
<!-- Internal DataTables css-->
<link href="{{asset('assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/datatable/responsivebootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/datatable/fileexport/buttons.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<style>
    .help-block{
        color: red;
    }
</style>
