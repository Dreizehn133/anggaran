<!-- Main Footer-->
<div class="main-footer text-right">
    <div class="container">
        <div class="row row-sm">
            <div class="col-md-12">
                <span>Copyright © 2021. v{!! config('static.appVer') !!}</span>
            </div>
        </div>
    </div>
</div>
<!--End Footer-->
