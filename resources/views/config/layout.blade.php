<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Anggaran">
    <meta name="author" content="Xyz">
{{--    <meta name="keywords" content="admin,dashboard,panel,bootstrap admin template,bootstrap dashboard,dashboard,themeforest admin dashboard,themeforest admin,themeforest dashboard,themeforest admin panel,themeforest admin template,themeforest admin dashboard,cool admin,it dashboard,admin design,dash templates,saas dashboard,dmin ui design">--}}

    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/brand/favicon.ico')}}" type="image/x-icon"/>

    <!-- Title -->
    <title>@yield('menu_title','Anggaran')</title>
    @livewireStyles
    @include('config.css')
    @yield('internal.css')
    <style>
        .main-sidebar-sticky{
            background: #0d3354 !important;
        }
        .main-body.leftmenu .sidemenu-logo{
            background: #0d3354 !important;
        }
        .main-sidebar-body .nav-item.active .nav-link:before, .main-sidebar-body .nav-item.active .nav-link:after {
            border-right: 20px solid #0d3354!important;
        }
        .main-sidebar-body .nav-link {
            color: #9999a2 !important;
        }
        .main-sidebar-body .nav-label {
            color: #c0c0c6 !important;
        }
        .main-sidebar-body .nav-sub-link {
            color: #9999a2 !important;
        }
        .main-content-title{
            color : #06258d !important;
        }
        .main-header{
            background-color: #f4f5f8 !important;
        }
        .main-footer{
            background-color: #f4f5f8 !important;
        }
    </style>
</head>

<body class="main-body leftmenu">

<!-- Loader -->
<div id="global-loader">
    <img src="{{asset('assets/img/loader.svg')}}" class="loader-img" alt="Loader">
</div>
<!-- End Loader -->


<!-- Page -->
<div class="page">

    <!-- Sidemenu -->
    <div class="main-sidebar main-sidebar-sticky side-menu">
        <div class="sidemenu-logo">
            <a class="main-logo" href="/">
                <img src="{{asset('assets/img/logos/logo.png')}}" class="header-brand-img desktop-logo" alt="logo" height="37px">
                <img src="{{asset('assets/img/logos/logo.png')}}" class="header-brand-img icon-logo mb-1" alt="logo" height="37px">
                <img src="{{asset('assets/img/logos/logo.png')}}" class="header-brand-img desktop-logo theme-logo" alt="logo" height="37px">
                <img src="{{asset('assets/img/logos/logo.png')}}" class="header-brand-img icon-logo theme-logo" alt="logo" height="37px">
            </a>
        </div>
        <div class="main-sidebar-body">
            @include('config.menu')
        </div>
    </div>
    <!-- End Sidemenu -->

    <!-- Main Header-->
    <div class="main-header side-header sticky">
        <div class="container-fluid">
            <div class="main-header-left">
                <a class="main-header-menu-icon" href="#" id="mainSidebarToggle"><span></span></a>
            </div>
            <div class="main-header-center">
                <div class="responsive-logo">
                    <a href="/"><img src="{{asset('assets/img/logos/logo.png')}}" class="mobile-logo p-5" alt="logo"></a>
                    <a href="/"><img src="{{asset('assets/img/logos/logo.png')}}" class="mobile-logo-dark" alt="logo"></a>
                </div>
            </div>
            <div class="main-header-right">
                <div class="dropdown d-md-flex">
                    <a class="nav-link icon full-screen-link" href="">
                        <i class="fe fe-maximize fullscreen-button fullscreen header-icons"></i>
                        <i class="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i>
                    </a>
                </div>

                <div class="dropdown main-profile-menu">
                    <a class="d-flex" href="">
                        <span class="main-img-user" ><img alt="avatar" src="{{asset('assets/img/svgs/crypto-currencies/flo.svg')}}"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="header-navheading">
                            <h6 class="main-notification-title">{{\Illuminate\Support\Facades\Auth::user()->nama}}</h6>
                            <p class="main-notification-text">{{\Illuminate\Support\Facades\Auth::user()->getSubditName->nama}}</p>
                        </div>

                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fe fe-power"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Header-->

    <!-- Mobile-header -->
    <div class="mobile-main-header">
        <div class="mb-1 navbar navbar-expand-lg  nav nav-item  navbar-nav-right responsive-navbar navbar-dark  ">
            <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                <div class="d-flex order-lg-2 ml-auto">
                    <div class="dropdown ">
                        <a class="nav-link icon full-screen-link">
                            <i class="fe fe-maximize fullscreen-button fullscreen header-icons"></i>
                            <i class="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i>
                        </a>
                    </div>
                    <div class="dropdown main-profile-menu">
                        <a class="d-flex" href="#">
                            <span class="main-img-user" ><img alt="avatar" src="{{asset('assets/img/svgs/crypto-currencies/flo.svg')}}"></span>

                        </a>
                        <div class="dropdown-menu">
                            <div class="header-navheading">
                                <h6 class="main-notification-title">{{\Illuminate\Support\Facades\Auth::user()->nama}}</h6>
                                <p class="main-notification-text">{{\Illuminate\Support\Facades\Auth::user()->getSubditName->nama}}</p>
                            </div>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-forms').submit();">
                                <i class="fe fe-power"></i> {{ __('Logout') }}
                            </a>

                            <form id="logout-forms" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile-header closed -->

    <!-- Main Content-->
    <div class="main-content side-content pt-0">

        <div class="container-fluid">
            <div class="inner-body">

                <!-- Page Header -->
                <div class="page-header">
                    <div>
                        <h2 class="main-content-title tx-24 mg-b-5">@yield('menu_title','')</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">@yield('path1','')</a></li>
                            <li class="breadcrumb-item active" aria-current="page">@yield('path2','')</li>
                        </ol>
                    </div>
                </div>
                <!-- End Page Header -->

                <!--Row-->
                <div class="row row-sm">
                    <div class="col-sm-12 col-lg-12 col-xl-12">
                        @yield('body')
                    </div><!-- col end -->
                </div><!-- Row end -->
            </div>
        </div>
    </div>
    <!-- End Main Content-->

    @include('config.footer')

</div>
<!-- End Page -->

<!-- Back-to-top -->
<a href="#top" id="back-to-top"><i class="fe fe-arrow-up"></i></a>
@yield('modal')
@livewireScripts
@include('config.js')
@yield('internal_js')
<script type="text/javascript">
    var table = null;
    $(function (){
        $('.select2').select2({width : '100%'})

    })
    // window.livewire.on('modal', () => {
    //     $('#mymodal').modal('hide');
    // });
    // window.livewire.on('datatable', () => {
    //     console.log('wwww');
    //     // table.clear().destroy();
    //     // table = $('#myDataTable').DataTable( {
    //     //     lengthChange: false,
    //     //     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    //     // } );
    //     // table.buttons().container()
    //     //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
    // });
    // // document.addEventListener("livewire:load", function (event) {
    // //     window.livewire.hook('afterDomUpdate', () => {
    // //         $('.select2').select2({
    // //             width : '100%'
    // //         });
    // //     });
    // // });
    // document.addEventListener("livewire:load", () => {
    //     Livewire.hook('message.processed', (message, component) => {
    //         console.log('hook')
    //         initial()
    //
    //     }); });
    // window.addEventListener('refreshComponent', event => {
    //     console.log(event.detail.componentName);
    //     // table = $(event.detail.componentName).DataTable( {
    //     //     lengthChange: false,
    //     //     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    //     // } );
    //     table.button().add( 0, {
    //         action: function ( e, dt, button, config ) {
    //             dt.ajax.reload();
    //         },
    //         text: 'Reload table'
    //     } );        table.buttons().container()
    //         .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
    //     console.log('Updated table : ' + event.detail.componentName)
    // })
</script>
</body>
</html>
