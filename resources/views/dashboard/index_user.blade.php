@extends('config.layout')
@section('menu_title','Dashboard')
@section('path1','Home')
@section('path2','Dashboard')
@section('body')
    <div class="row row-sm">
        @foreach($pengumuman as $p)
            <div class="col-md-3">
                <div class="card custom-card" style="cursor: pointer" onclick="showPengumuman({{$p->id}})">
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <span class="text-muted tx-13 mb-3">{{date('d/m/Y H.i',strtotime($p->limit))}}</span>
                                <h3 class="m-0 mt-2">{{$p->judul}}</h3>
                            </div>
                            <div class="ml-auto mt-auto">
                                <img src="assets/img/svgs/crypto-currencies/btc.svg" class="wd-30 hd-30 mr-2" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('modal')
    <div class="modal" id="mymodal2">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title"><span id="titles"></span></h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <p>Until : <span id="expireds"></span></p>
                        <div id="isis"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('internal_js')
    <script>
        function showPengumuman(id) {
            $.ajax({
                type: 'GET',
                url: '{{route('pengumuman.data')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#titles').text(data.judul)
                    $('#expireds').text(data.expired)
                    $('#isis').html(data.isi)
                    $('#mymodal2').modal()
                }
            })
        }
    </script>
@endsection

