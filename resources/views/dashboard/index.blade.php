@extends('config.layout')
@section('menu_title','Dashboard')
@section('path1','Home')
@section('path2','Dashboard')
@section('internal.css')
    <link href="/assets/plugins/summernote/summernote-bs4.css" rel="stylesheet">
@endsection
@section('body')
    <div class="row row-sm">
        <div class="pull-right">
            <button class="btn btn-outline-primary" data-effect="effect-fall" data-toggle="modal" href="#mymodal"><i
                    class="fa fa-plus-circle"></i>&nbsp;Pengumuman
            </button>
        </div>
    </div>
    <br/>
    <div class="row row-sm" id="pengumumankonten">
{{--        @foreach($pengumuman as $p)--}}
{{--            <div class="col-md-3">--}}
{{--                <div class="card custom-card" style="cursor: pointer" onclick="showPengumuman({{$p->id}})">--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="d-flex no-block align-items-center">--}}
{{--                            <div>--}}
{{--                                <span class="text-muted tx-13 mb-3">{{date('d/m/Y H.i',strtotime($p->limit))}}</span>--}}
{{--                                <h3 class="m-0 mt-2">{{$p->judul}}</h3>--}}
{{--                            </div>--}}
{{--                            <div class="ml-auto mt-auto">--}}
{{--                                <img src="assets/img/svgs/crypto-currencies/btc.svg" class="wd-30 hd-30 mr-2" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--        <div ></div>--}}
    </div>
    <div class="row row-sm">
        <div class="row align-items-center">
            <div class="col-md-4">
                <label class="mg-b-0">Tahun</label>
            </div>
            <div class="col-md-8 mg-md-t-0">
                <select class="form-control select2" id="tahun" style="width: 100%">
                    @foreach($years as $y)
                        <option value="{{$y}}" {!! $y == date('Y') ? 'selected' : '' !!}>{{$y}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <br/>

    <div class="row row-sm">
        <div class="col-sm-6 col-md-6 col-xl-3">
            <div class="card custom-card">
                <div class="card-body text-center">
                    <div class="icon-service bg-primary-transparent rounded-circle text-primary">
                        <i class="fe fe-layers"></i>
                    </div>
                    <p class="mb-1 text-muted">Total Pagu</p>
                    <h3 class="mb-0">
                        <span>Rp. </span><span id="totalpagu"></span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-xl-3">
            <div class="card custom-card">
                <div class="card-body text-center">
                    <div class="icon-service bg-info-transparent rounded-circle text-info">
                        <i class="fe fe-dollar-sign"></i>
                    </div>
                    <p class="mb-1 text-muted">Total Anggaran</p>
                    <h3 class="mb-0">
                        <span>Rp. </span><span id="totalanggaran"></span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-sm" id="components">

    </div>
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Tambahkan Pengumuman</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                    <div class="modal-body">
                        <input type="hidden" id="ids" name="ids"/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="title-group">
                                    <label>Judul</label>
                                    <input class="form-control" id="title" name="title" type="text"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="expired-group">
                                    <label>Batas Waktu</label>
                                    <input class="form-control" id="expired" name="expired" type="datetime-local"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="ket-group">
                            <label>Isi Pengumuman</label>
                            <textarea id="summernote" name="ket" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn ripple btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal2">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title"><span id="titles"></span></h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                   <div class="col-md-12">
                       <p>Until : <span id="expireds"></span></p>
                       <div id="isis"></div>
                   </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('internal_js')
    <script src="/assets/plugins/summernote/summernote-bs4.js"></script>

    <script>
        $('#summernote').summernote({
            placeholder: 'Ketik Pengumuman Anda Disini',
            tabsize: 3,
            height: 300
        });
        $(function (){
            getPengumuman();
            getDashboard();
        })
        function getDashboard(){
            $.ajax({
                type: 'GET',
                url: '{{route('home')}}/'+$('#tahun option:selected').val(),
                dataType: 'json',
                success: function (data) {
                    $('#components').html(data.view)
                    $('#totalpagu').html(data.totalpagu)
                    $('#totalanggaran').html(data.totalanggaran)
                }
            })
            $('#totalanggaran').html()
        }
        $('#tahun').change(function (e){
            getDashboard();
        })
        $('#form').submit(function (even) {
            even.preventDefault();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) => {
                    if (!val) {
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('pengumuman.post')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            // reset()
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            getPengumuman();
                            $('#mymodal').modal('hide')
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        $('#mymodal').on('hidden.bs.modal', function (e) {
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            $('#form')[0].reset()
        });

        function showPengumuman(id) {
            $.ajax({
                type: 'GET',
                url: '{{route('pengumuman.data')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $('#titles').text(data.judul)
                    $('#expireds').text(data.expired)
                    $('#isis').html(data.isi)
                    $('#mymodal2').modal()
                }
            })
        }
        function getPengumuman(){
            $('#pengumumankonten').empty()
            $.ajax({
                type: 'GET',
                url: '{{route('pengumuman.data')}}',
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $.each(data, function (key, value) {
                        $('#pengumumankonten').append(cardData(value))
                    });
                }
            })
        }
        function cardData(data){
            return '<div class="col-md-3">' +
                    '<div class="card custom-card" style="cursor: pointer" onclick="showPengumuman('+data.id+')">' +
                        '<div class="card-body">' +
                            '<div class="d-flex no-block align-items-center">' +
                                '<div>' +
                                    '<span class="text-muted tx-13 mb-3">'+data.limit+'</span>' +
                                    '<h3 class="m-0 mt-2">'+data.judul+'</h3' +
                                '</div>' +
                '</div>' +
                                '<div class="ml-auto mt-auto">' +
                                    '<img src="assets/img/svgs/crypto-currencies/dash.svg" class="wd-30 hd-30 mr-2" alt="">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        }
    </script>
@endsection
{{--<div class="col-md-3">--}}
{{--    <div class="card custom-card" style="cursor: pointer" onclick="showPengumuman({{$p->id}})">--}}
{{--        <div class="card-body">--}}
{{--            <div class="d-flex no-block align-items-center">--}}
{{--                <div>--}}
{{--                    <span class="text-muted tx-13 mb-3">{{date('d/m/Y H.i',strtotime($p->limit))}}</span>--}}
{{--                    <h3 class="m-0 mt-2">{{$p->judul}}</h3>--}}
{{--                </div>--}}
{{--                <div class="ml-auto mt-auto">--}}
{{--                    <img src="assets/img/svgs/crypto-currencies/btc.svg" class="wd-30 hd-30 mr-2" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
