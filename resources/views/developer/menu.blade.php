@extends('config.layout')
@section('menu_title','Menu')
@section('path1','Developer')
@section('path2','Menu')
@section('body')
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card overflow-hidden">

                <div class="card-body">
                    <div class="card-title">
                        <div class="pull-left">
                            <h6 class="main-content-label mb-1">Menu</h6>
                            <p class="text-muted card-sub-title">&nbsp;</p>
                        </div>
                        <div class="pull-right">
                            <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-fall" data-toggle="modal" href="#mymodal">Add New</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="myDataTable" class="table table-bordered border-t0 key-buttons text-nowrap w-100" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Parent Menu</th>
                                <th>Nama</th>
                                <th>Icon</th>
                                <th>Url</th>
                                <th>Is Label?</th>
                                <th>Is Header?</th>
                                <th>Index</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--                            @foreach($data as $index => $d)--}}
                            {{--                                <tr>--}}
                            {{--                                    <td>{{$index + 1}}</td>--}}
                            {{--                                    <td>{{$d->kode}}</td>--}}
                            {{--                                    <td>{{$d->nama}}</td>--}}
                            {{--                                    <td>@if($d->flag)<span class="badge badge-pill badge-primary">Header</span>--}}
                            {{--                                        @else--}}
                            {{--                                            <span class="badge badge-pill badge-secondary">Detail</span>--}}
                            {{--                                        @endif--}}
                            {{--                                    </td>--}}
                            {{--                                    <td>{{$d->user}}</td>--}}
                            {{--                                    <td></td>--}}
                            {{--                                </tr>--}}
                            {{--                            @endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @livewire('gl-account')--}}
@endsection
@section('modal')
    <div class="modal" id="mymodal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Menu</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="form" method="POST">
                    <div class="modal-body">
                        <div class="form-group" id="title-group">
                            <p class="mg-b-10">Is Label?</p>
                            <div class="row mg-t-10">
                                <div class="col-lg-3">
                                    <label class="rdiobox"><input name="title" type="radio" value="1"> <span>Label</span></label>
                                </div>
                                <div class="col-lg-3 mg-t-20 mg-lg-t-0">
                                    <label class="rdiobox"><input checked="" name="title" type="radio" value="0"> <span>Menu</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="name-group">
                            <p class="mg-b-10">Name</p>
                            <input type="text" class="form-control" placeholder="Menu Name" id="name" name="name">
                        </div>
                        <div class="form-group" id="index-group">
                            <p class="mg-b-10">Index</p>
                            <input type="number" class="form-control" placeholder="Menu Index" id="index" name="index">
                        </div>
                        <div id="menus">
                            <div class="form-group" id="header-group">
                                <p class="mg-b-10">Header Menu</p>
                                <select class="form-control select2" id="header" name="header">
                                    <option value="0">Is Header Menu</option>
                                </select>
                            </div>
                            <div class="form-group" id="title_id-group">
                                <p class="mg-b-10">Title Menu</p>
                                <select class="form-control select2" id="title_id" name="title_id">
                                    <option value="0">No Title</option>
                                </select>
                            </div>
                            <div class="form-group" id="url-group">
                                <p class="mg-b-10">url</p>
                                <input type="text" class="form-control" placeholder="ex: /url" id="url" name="url">
                            </div>
                            <div class="form-group" id="icon-group">
                                <p class="mg-b-10">Icon</p>
                                <input type="text" class="form-control" placeholder="ex : fa fa-icon" id="icon" name="icon">
                                <input type="hidden" id="ids" name="ids"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn ripple btn-primary" type="submit" >Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('internal_js')
    <script>
        var table;
        $(function (){
            var nowD = '{{ date('Y-m-d H:i:s') }}';
            getHeader();
            table = $('#myDataTable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                colReorder: true,
                ajax: {
                    url: '{{route('developer.menu.data')}}',
                    // data: function (e) {
                    //     e.branch = $('#filter-br').val()
                    // }
                },
                columns: [
                    {data: 'index', orderable: false, searchable: false},
                    {data: 'parent_name',name :'m.name'},
                    {data: 'name',name :'menu.name'},
                    {data: 'icon',name :'menu.icon'},
                    {data: 'url',name :'menu.url'},
                    {data: 'title',name :'menu.title'},
                    {data: 'header',name :'menu.header'},
                    {data: 'urutan',name :'menu.index'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                lengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
                dom: "<'row'<'col-sm-3'B><'col-sm-2'l><'col-sm-7'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // colvis: {text: 'Set Columns'},
                buttons: [
                    {
                        extend: 'print',
                        title: document.title,
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        messageBottom: 'Export on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        filename: document.title + ' ' + nowD,
                        title: document.title,
                        orientation: 'potrait',
                        pageSize: 'LEGAL',
                        messageBottom: 'Printed on ' + nowD,
                        exportOptions: {
                            columns: 'th:not(.no-print)'
                        }
                    },
                    // {
                    //     extend: 'colvis',
                    //     text: 'Set Columns',
                    // }
                ]
            });
            // table.buttons().container()
            //     .appendTo( '#myDataTable_wrapper .col-md-6:eq(0)' );
        })
        function getHeader(){
            $.ajax({
                type: 'GET',
                url: '{{route('developer.menu.header')}}',
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    $('#header').find('option').not(':first').remove()
                    $.each(data.header, function (key, value) {
                        $('#header').append('<option value="' + value.id + '">'+ value.name + '</option>')
                    });
                    $('#title_id').find('option').not(':first').remove()
                    $.each(data.title, function (key, value) {
                        $('#title_id').append('<option value="' + value.id + '">'+ value.name + '</option>')
                    });
                },
                error: function (request, status, error) {
                    console.log(request.responseText)
                    json = $.parseJSON(request.responseText);
                    if (typeof json !== "string") {
                        $.each(json, function (key, value) {
                            $('#' + key + '-group').addClass('has-error');
                            $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                        });
                    } else {
                        toastr.error(request.responseText, error, {timeOut: 5000});

                    }
                }
            });

        }
        $('#form').submit(function (even) {
            even.preventDefault();
            var formData = new FormData($(this)[0]);
            formData.append('_token', '{{csrf_token()}}')
            swal({
                title: "Are you sure?",
                text: 'Continue Action ?',
                type: 'warning',
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                button: "Yes, Continue Action",
                // closeOnConfirm: true,
            }).then((val) =>
                {
                    if(!val){
                        return;
                    }
                    $('.form-group').removeClass('has-error'); // remove the error class
                    $('.help-block').remove(); // remove the error text
                    $.ajax({
                        type: 'POST',
                        url: '{{route('developer.menu.add')}}',
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function (data) {
                            // reset()
                            toastr.success(data, 'Sukses!', {timeOut: 5000});
                            getHeader();
                            $('#mymodal').modal('hide')
                            table.ajax.reload()
                        },
                        error: function (request, status, error) {
                            var json = JSON.parse(request.responseText);
                            if (typeof json !== "string") {
                                $.each(json, function (key, value) {
                                    console.log(key)
                                    console.log(value)
                                    $('#' + key + '-group').addClass('has-error');
                                    $('#' + key + '-group').append('<span class="help-block">' + value + '</span>');
                                });
                            } else {
                                toastr.error(request.responseText, error, {timeOut: 5000});
                            }
                        }
                    });
                }
            );
        });
        function reset(){
            $('#form')[0].reset()
            $('.select2').val('0').trigger('change')
            $('#ids').val('')
            $('#menus').show()
        }
        function edit(id){
            $('#ids').val(id)
            $.ajax({
                type: 'GET',
                url: '{{route('developer.menu.header')}}/' + id,
                dataType: 'json',
                success: function (data) {
                    $('input:radio[name="title"]').filter('[value="'+data.title+'"]').attr('checked', true).trigger('click');
                    // $('input:radio[name=title]').val(data.title).trigger('change')
                    $('#name').val(data.name)
                    $('#icon').val(data.icon)
                    $('#index').val(data.index)
                    $('#url').val(data.url)
                    $('#header').val(data.header_id != null ? data.header_id : '0').trigger('change')
                    $('#title_id').val(data.title_id != null ? data.title_id : '0').trigger('change')
                    $('#mymodal').modal()
                }
            })
        }
        function remove(id){
            swal({
                title: "Are you sure?",
                text: "You will delete data!",
                type: "warning",
                // showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete!",
                // closeOnConfirm: true
            }).then((val) => {
                if(!val)return;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('developer.menu.delete') }}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'ids': id
                    },
                    dataType: 'json',
                    success: function (data) {
                        toastr.success(data, 'Sukses!', {timeOut: 5000});
                        // toastr.success(data, 'Success', {timeOut: 5000});
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        json = $.parseJSON(request.responseText);
                        toastr.error(json.errors, 'Error', {timeOut: 5000});
                    }
                })
            })
        }
        $('#mymodal').on('hidden.bs.modal', function (e) {
            // $(this)
            //     .find("input,textarea")
            //     .val('')
            //     .end()
            //     .find("select")
            //     .val('0')
            //     .trigger('change')
            //     .end()
            //     .find("input[type=checkbox]")
            //     .prop("checked", "")
            //     .end();
            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove();
            reset();// remove the error text
        });
        $('input:radio[name=title]').click(function (){
            console.log($(this).val())
            if($(this).val() == 1){
                $('#menus').hide()
            }else{
                $('#menus').show()
            }
        })
    </script>
@endsection
