<?php

namespace Database\Seeders;

use App\Models\MSubdit;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $subdit = new MSubdit();
//        $subdit->kode = '01';
//        $subdit->nama = 'SUBDIT DUMMY';
//        $subdit->active = '1';
//        $subdit->save();
//        $usr = new User();
//        $usr->email = 'USER@anggaran.id';
//        $usr->password = Hash::make('qwerty');
//        $usr->kode_subdit = '01';
//        $usr->nama = 'User 01';
//        $usr->active = '1';
//        $usr->role = 'B';
//        $usr->save();
        \Log::debug(Hash::make('qwerty'));
    }
}
