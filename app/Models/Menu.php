<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $table = 'menu';
    public $timestamps = false;
    protected $fillable = ['active'];
    function get_menu(){
        return $this->hasMany(Menu::class,'title_id','id')
            ->join('menu_user as mu',function ($e){
                return $e->on('mu.menu_id','menu.id')
                    ->where('mu.role',\Illuminate\Support\Facades\Auth::user()->role)
                    ->where('mu.active',true);
            })
            ->where('menu.active',true)->orderBy('menu.index')
            ->select(['menu.*']);
    }
    function get_child(){
        return $this->hasMany(Menu::class,'header_id','id')
            ->join('menu_user as mu',function ($e){
                return $e->on('mu.menu_id','menu.id')
                    ->where('mu.role',\Illuminate\Support\Facades\Auth::user()->role)
                    ->where('mu.active',true);
            })
            ->where('menu.active',true)->orderBy('menu.index')
            ->select(['menu.*']);
    }
}
