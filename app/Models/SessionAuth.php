<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionAuth extends Model
{
    protected $table='sessions';
    protected $guarded=[];
    public $timestamps = false;
}
