<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MGlAccount extends Model
{
    use HasFactory;
    protected $table = 'm_gl_account';
    protected $fillable = ['active'];
}
