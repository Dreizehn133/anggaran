<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnggaranDetail extends Model
{
    use HasFactory;
    protected $table = 'anggaran_detail';
    public $timestamps = false;
}
