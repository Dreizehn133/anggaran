<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AnggaranRPD extends Model
{
    public $timestamps=false;
    protected $guarded = [];
    protected $table = 'anggaran_rpd';
}
