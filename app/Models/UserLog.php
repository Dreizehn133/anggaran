<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    public $timestamps=false;
    protected $guarded = [];
    protected $table= 'user_log';
    protected $casts = [
        'created_at' => 'datetime:d/m/Y H.i'
    ];
}
