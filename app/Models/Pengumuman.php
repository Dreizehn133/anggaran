<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table = 'pengumuman';
    protected $casts = [
      'limit' => 'datetime:d/m/Y H.i'
    ];
}
