<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnggaranLog extends Model
{
    protected $table = 'anggaran_log';
    protected $guarded = [];
    public $timestamps = false;
}
