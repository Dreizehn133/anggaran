<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnggaranDetailCatatan extends Model
{
    protected $table = 'anggaran_detail_catatan';
    protected $guarded = [];
    public $timestamps = false;
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i',
    ];
}
