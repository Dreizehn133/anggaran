<?php

namespace App\Exports;

use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\MGlAccount;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Worksheet\BaseDrawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AnggaranExport implements FromView, WithColumnWidths, WithEvents, WithProperties, WithDrawings
{
    public $id;

    /**
     * AnggaranExport constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
    }

    public function view(): View
    {
        $header = Anggaran::join('m_subdit as ms','ms.kode','anggaran.kode_subdit')->where('anggaran.id',base64_decode($this->id))->first(['anggaran.*','ms.nama','ms.leader','ms.nip']);
        $data = AnggaranDetail::join('m_gl_account as gl','gl.kode','anggaran_detail.kode_gl')
            ->where('anggaran_detail.id_anggaran',base64_decode($this->id))
            ->where('anggaran_detail.active',true)
            ->orderBy('anggaran_detail.kode_gl')
            ->groupBy('anggaran_detail.id')
            ->get(['anggaran_detail.*','gl.nama',DB::raw('SUM(anggaran_detail.harga * anggaran_detail.var1 * anggaran_detail.var2 * anggaran_detail.var3 * anggaran_detail.var4) as jumlah')]);
        $gl = MGlAccount::where('active',true)->orderBy('kode')->get();
        Log::debug(count($gl));
        return view('anggaran.detail.excel',compact('header','data','gl'));
    }

    public function columnWidths(): array
    {
        return [
          'A'=>7,
          'B'=>5,
          'C'=>10,
          'D'=>40,
          'E'=>10,
          'F'=>7,
          'G'=>2,
          'H'=>10,
          'I'=>7,
          'J'=>30,
          'K'=>30,
          'L'=>30,
          'M'=>30,
        ];
    }

    public function properties(): array
    {
        return [
            'creator' => 'Rekap',
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A7:M'.((int)$event->sheet->getHighestRow() - 10),
                    [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'C8:C'.$event->sheet->getHighestRow(),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'D8:D'.$event->sheet->getHighestRow(),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'I8:I'.$event->sheet->getHighestRow(),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'J8:J'.$event->sheet->getHighestRow(),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'K8:K'.((int)$event->sheet->getHighestRow() - 10),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'L8:L'.$event->sheet->getHighestRow(),
                    [
                        'borders' => [
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'A7:M7',
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
            },
        ];
    }

    public function drawings()
    {
        $gl = MGlAccount::where('active',true)->orderBy('kode')->count();
        $gl += 13;
        $qr = QrCode::format('png')->size(150)->generate(route('anggaran.detail.preview',[$this->id]));
        $drawing = new MemoryDrawing();
        $drawing->setName('Rekap');
        $drawing->setImageResource(imagecreatefromstring($qr));
        $drawing->setCoordinates('B'.$gl);
        return $drawing;
    }
}
