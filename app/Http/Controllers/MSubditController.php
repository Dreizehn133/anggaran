<?php

namespace App\Http\Controllers;

use App\Models\MGlAccount;
use App\Models\MSubdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class MSubditController extends Controller
{
    function index(){
        return view('master.subdit.index');
    }
    function data(Request $request){
        $data = MSubdit::where('m_subdit.active',true)
            ->join('users as us','us.id','m_subdit.created_by')
            ->orderBy('m_subdit.kode')
            ->select(['m_subdit.*','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['action'])->make(true)
            ;
    }
    function post(Request $request){
        $rules = [
            'nama' => 'required',
            'kode' => 'required',
            'leader' => 'required',
            'nip' => 'required',
            'jabatan' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if(isset($request->ids)){
            $data = MSubdit::find(base64_decode($request->ids));
        }else{
            $data = new MSubdit();
        }
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->leader = $request->leader;
        $data->nip = $request->nip;
        $data->jabatan = $request->jabatan;
        $data->active = true;
        $data->created_by = \Auth::id();
        $data->save();
        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(MSubdit::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
        MSubdit::find($id)->update(['active'=>false]);
        return response()->json('Sukses');
    }
}
