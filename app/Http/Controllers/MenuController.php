<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MGlAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class MenuController extends Controller
{
    function index(){
        return view('developer.menu');
    }
    function data(Request $request){
        $data = Menu::where('menu.active',true)
            ->leftJoin('menu as m',function ($e){
                return $e->on('m.id','menu.header_id')
                    ->where('menu.header_id','<>',null);
            })
            ->orderBy('menu.title','desc')
//            ->orderBy('menu.header','desc')
//            ->orderBy('menu.name')
            ->select(['menu.*','m.name as parent_name','m.url as parenturl']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>';
            })
            ->addColumn('urutan',function ($e){
                return $e->index;
            })
            ->editColumn('title',function ($e){
                return $e->title == '1' ?
                    '<span class="badge badge-pill badge-primary">Label</span>'
                    :'<span class="badge badge-pill badge-secondary">Menu</span>'
                    ;
            })
            ->editColumn('header',function ($e){
                if($e->title == '1'){
                    return  '';
                }
                return $e->header == '1' ?
                    '<span class="badge badge-pill badge-primary">Header Menu</span>'
                    :'<span class="badge badge-pill badge-secondary">Menu</span>'
                    ;
            })
            ->editColumn('url',function ($e){
                return $e->parenturl.$e->url;
            })
            ->editColumn('icon',function ($e){
                return $e->icon == null ?
                    ''
                    :'<span class="'.$e->icon.'"></span>&nbsp;'.$e->icon
                    ;
            })
            ->rawColumns(['action','title','header','icon'])->make(true)
            ;
    }
    function header(){
        return response()->json(
          ['header'=>Menu::where('header',true)->where('active',true)->where('title',false)->get(),
              'title' => Menu::where('title',true)->where('active',true)->get()
          ]
        );
    }
    function post(Request $request){
        Log::debug($request);
        $rules = [
            'name' => 'required',
            'index' => 'required',
        ];
        if($request->title == '0'){
            $rules['url'] = 'required';
            if($request->header == '0'){
                $rules['icon'] = 'required';
            }
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        if(isset($request->ids)) {
            $data = Menu::find(base64_decode($request->ids));
        }else{
            $data = new Menu();
        }
        $data->name = $request->name;
        $data->index = $request->index;
        $data->title = $request->title;

        $data->icon = $request->icon;
        $data->active = true;
        if($request->title == '1'){
            $data->header = '0';
            $data->url = null;
        }else{
            $data->header = $request->header == '0' ? true : false;
            if($data->header){
                $data->title_id = $request->title_id;
            }
            $url = $request->url;
            $data->url = $url[0] != '/' ? '/'.$url : $url;
        }
        $data->header_id = $request->header != '0' ? $request->header : null;
        $data->save();
        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(Menu::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
        Menu::find($id)->update(['active'=>false]);
        return response()->json('Sukses');
    }

}
