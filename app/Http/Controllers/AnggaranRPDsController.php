<?php

namespace App\Http\Controllers;

use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\AnggaranRPD;
use App\Models\MSubdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class AnggaranRPDsController extends Controller
{
    public function index(){
        $subdit = MSubdit::where('active',true);
        if(Auth::user()->get_role_name->level != '1'){
            $subdit = $subdit->where('kode',Auth::user()->kode_subdit);
        }
        $subdit = $subdit->get();
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));

        return view('rpd.index',compact('years','subdit'));
    }

    public function data(Request $request){
        $data = AnggaranDetail::join('anggaran','anggaran.id','anggaran_detail.id_anggaran')
            ->join('m_subdit as ms','ms.kode','anggaran.kode_subdit')
            ->leftJoin('users as us','us.id','anggaran_detail.created_by')
            ->where('anggaran.active',true)
            ->where('anggaran_detail.active',true)
            ->where('anggaran.tahun',$request->tahun)
            ->where('anggaran.kode_subdit','like',$request->subdit.'%')
            ->orderBy('anggaran.kode_subdit')->orderBy('anggaran_detail.kode_gl')->select(['anggaran_detail.*','ms.nama','ms.kode','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('jumlah',function ($e){
                return number_format($e->var1*($e->var2?? 1)*($e->var3??1)*($e->var4??1));
            })
            ->editColumn('harga',function ($e){
                return number_format($e->harga);
            })
            ->addColumn('total',function ($e){
                return number_format(($e->var1*($e->var2?? 1)*($e->var3??1)*($e->var4??1)) * $e->harga);
            })
            ->addColumn('total_rpd',function ($e){
                $dt = AnggaranRPD::where('id_anggaran_detail',$e->id)->where('active',true)->first(\DB::raw('SUM(jumlah*harga) as total'))->total;
                return number_format($dt);
            })
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="showRPD(\'' . base64_encode($e->id) . '\')"> RPD</button>';
            })
            ->addColumn('files',function ($e){
                return $e->rpd != null? '<a target="_blank" class="btn btn-xs btn-success" href="'.route('rpd.download',[base64_encode($e->id)]).'"><i class="fa fa-file-signature"></i></a>':'<span class="badge bg-danger">N/A</span>';
            })
            ->rawColumns(['action','files'])->make(true);
    }
    public function post(Request $request){
        AnggaranRPD::create([
           'id_anggaran_detail'=>base64_decode($request->ids),
           'jumlah'=>$request->jumlah,
           'harga'=>$request->harga,
           'keterangan'=>$request->keterangan,
           'created_by'=>Auth::id(),
           'created_at'=>date('Y-m-d H:i:s'),
        ]);
        return response()->json(['message'=>'Sukses menambahkann']);
    }
    public function dataRPD(Request $request){
        $data = AnggaranRPD::where('id_anggaran_detail',base64_decode($request->ids))->where('active',true);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('jumlah',function ($e){
                return number_format($e->jumlah);
            })
            ->editColumn('harga',function ($e){
                return number_format($e->harga);
            })
            ->addColumn('total',function ($e){
                return number_format($e->jumlah * $e->harga);
            })
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-danger" type="button" onclick="deleteRPD(\'' . base64_encode($e->id) . '\')"> <i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['action'])->make(true);
    }
    public function removeRPD(Request $request){
        AnggaranRPD::where('id',base64_decode($request->ids))->update(['active'=>false]);
        return response()->json(['message'=>'Sukses Menghapus']);

    }
    function upload(Request $request){
        Log::debug($request);
        $data = AnggaranDetail::find(base64_decode($request->ids));
        $format = $request->file->getClientOriginalExtension();
        $files = 'rpd.pdf';
        if($request->file('file') != null){
            $dest = public_path('berkas/rpd/'.$request->ids);
            $request->file('file')->move($dest,$files);
        }
        $data->rpd = $dest.'/'.$files;
        $data->save();
        return response()->json('Sukses Upload ');
    }
    public function download($id){
        $data = AnggaranDetail::find(base64_decode($id));
        return  response()->download($data->rpd,'rpd.pdf');
    }
}
