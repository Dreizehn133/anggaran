<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PengumumenController extends Controller
{
    public function data(){
        $pengumuman = Pengumuman::where('limit','>=',date('Y-m-d H:i:s'))->get();
        return response()->json($pengumuman);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'expired' => 'required',
            'ket' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $data = new Pengumuman();
        $data->judul = $request->title;
        $data->limit = $request->expired;
        $data->isi = $request->ket;
        $data->save();
        return response()->json('Sukses Tambah Pengumuman');
    }

    public function detail($id)
    {
        $data = Pengumuman::find($id);
        $data->expired = date('d/m/Y H.i',strtotime($data->limit));
        return response()->json($data);
    }

    public function edit(Pengumuman $pengumuman)
    {
        //
    }

    public function update(Request $request, Pengumuman $pengumuman)
    {
        //
    }

    public function destroy(Pengumuman $pengumuman)
    {
        //
    }
}
