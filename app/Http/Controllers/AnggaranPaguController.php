<?php

namespace App\Http\Controllers;

use App\Models\AnggaranSubdit;
use App\Models\Menu;
use App\Models\MSubdit;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AnggaranPaguController extends Controller
{
    function index(){
        $subdit = MSubdit::where('active',true);
        if(Auth::user()->role != env('ROLE_ADMIN')){
            $subdit = $subdit->where('kode',Auth::user()->kode_subdit);
        }
        $subdit = $subdit->get();
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));

        return view('anggaran.pagu.index',compact('years','subdit'));
    }
    function data(Request $request){
        $data = AnggaranSubdit::join('m_subdit as ms',function ($e){
                return $e->on('ms.kode','anggaran_subdit.kode_subdit');
            })
            ->where('anggaran_subdit.tahun',$request->tahun)
            ->select(['anggaran_subdit.*','ms.nama as subdit','ms.kode as kode']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('jumlah',function ($e){
                return number_format($e->jumlah);
            })
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['action'])->make(true)
            ;
    }
    function header(){
        return response()->json(
            ['header'=>Menu::where('header',true)->where('active',true)->where('title',false)->get(),
                'title' => Menu::where('title',true)->where('active',true)->get()
            ]
        );
    }
    function post(Request $request){
        $rules = [
            'jumlah' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        if(isset($request->ids)) {
            $data = AnggaranSubdit::find(base64_decode($request->ids));
        }else{
            $data = new AnggaranSubdit();
            $check = AnggaranSubdit::where('kode_subdit',$request->subdit)->where('tahun',$request->tahun)->first();
            if($check != null){
                return response()->json([
                    'subdit'=>['Pagu Telah Didaftar untuk subdit dan tahun yang dipilih!'],
                    'tahun'=>['Pagu Telah Didaftar untuk subdit dan tahun yang dipilih!']
                ],400);
            }
        }
        $data->kode_subdit = $request->subdit;
        $data->tahun = $request->tahun;
        $data->jumlah = $request->jumlah;
        $data->save();
        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(AnggaranSubdit::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
        AnggaranSubdit::find($id)->delete();
        return response()->json('Sukses');
    }
}
