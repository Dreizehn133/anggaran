<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuRole;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class MenuRoleController extends Controller
{
    function index(){
        $role = UserRole::all();
        $menu = Menu::where('active',true)->get();
        return view('developer.menu_role',compact('role','menu'));
    }
    function data(Request $request){
        $data = MenuRole::where('menu_user.active',true)
            ->join('menu as m',function ($e){
                return $e->on('m.id','menu_user.menu_id');
            })
            ->join('user_role as ur',function ($e){
                return $e->on('ur.kode','menu_user.role');
            })
            ->select(['menu_user.*','ur.nama as role','m.name as menu']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['action'])->make(true)
            ;
    }
    function header(){
        return response()->json(
            ['header'=>Menu::where('header',true)->where('active',true)->where('title',false)->get(),
                'title' => Menu::where('title',true)->where('active',true)->get()
            ]
        );
    }
    function post(Request $request){
        Log::debug($request);

        if(isset($request->ids)) {
            $data = MenuRole::find(base64_decode($request->ids));
        }else{
            $data = new MenuRole();
            $check = MenuRole::where('role',$request->role)->where('menu_id',$request->menu)->where('active',true)->first();
            if($check != null ){
                return response()->json(['role'=>["Role ini telah terdaftar"],'menu'=>['Menu ini telah terdaftar untuk role yg dipilih!']],400);
            }
        }
        $data->role = $request->role;
        $data->menu_id = $request->menu;
        $data->active = true;
        $data->save();
        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(MenuRole::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
        MenuRole::find($id)->update(['active'=>false]);
        return response()->json('Sukses');
    }
}
