<?php

namespace App\Http\Controllers;

use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\MSubdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RKAReportController extends Controller
{
    public function index()
    {
        $subdit = MSubdit::where('active',true);
        if(Auth::user()->get_role_name->level != '1'){
            $subdit = $subdit->where('kode',Auth::user()->kode_subdit);
        }
        $subdit = $subdit->get();
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));

        return view('rka_report.index',compact('years','subdit'));
    }
    function data(Request $request){
        $data = AnggaranDetail::join('anggaran','anggaran.id','anggaran_detail.id_anggaran')
            ->join('m_subdit as ms','ms.kode','anggaran.kode_subdit')
            ->leftJoin('users as us','us.id','anggaran_detail.created_by')
            ->where('anggaran.active',true)
            ->where('anggaran_detail.active',true)
            ->where('anggaran.tahun',$request->tahun)
            ->orderBy('anggaran.kode_subdit')->orderBy('anggaran_detail.kode_gl')->select(['anggaran_detail.*','ms.nama','ms.kode','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('jumlah',function ($e){
                return number_format($e->var1*($e->var2?? 1)*($e->var3??1)*($e->var4??1));
            })
//            ->editColumn('nama',function ($e){
//                return $e->kode. ' / '. $e->nama;
//            })
            ->editColumn('harga',function ($e){
                return number_format($e->harga);
            })
            ->addColumn('files',function ($e){
                $btn = '';
                $btn .= $e->kak != '0'?'&nbsp;<a class="badge badge-pill badge-success" target="_blank" href="'.route('anggaran.download.detail',['KAK',base64_encode($e->id)]).'">KAK <i class="fa fa-check-circle"></i></a>':'&nbsp;<button class="badge badge-pill badge-danger" onclick="uploadDetail(\''.'KAK'.'\''.',\''.base64_encode($e->id).'\')">KAK <i class="fa fa-times-circle"></i></button>';
                $btn .= $e->rab != '0'?'&nbsp;<a class="badge badge-pill badge-success" target="_blank" href="'.route('anggaran.download.detail',['RAB',base64_encode($e->id)]).'">RAB <i class="fa fa-check-circle"></i></a>':'&nbsp;<button class="badge badge-pill badge-danger" onclick="uploadDetail(\''.'RAB'.'\''.',\''.base64_encode($e->id).'\')">RAB <i class="fa fa-times-circle"></i></button>';
                return $btn;
            })
            ->rawColumns(['files'])->make(true);
    }

}
