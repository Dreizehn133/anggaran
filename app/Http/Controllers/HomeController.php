<?php

namespace App\Http\Controllers;

use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\AnggaranSubdit;
use App\Models\Pengumuman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(\Auth::user()->role != env('ROLE_ADMIN')){
            $pengumuman = Pengumuman::where('limit','>=',date('Y-m-d H:i:s'))->get();
            return view('dashboard.index_user',compact('pengumuman'));
        }
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));
        return view('dashboard.index',compact('years'));
    }
    function getFilterDashboard($y){
        $data = Anggaran::join('m_subdit as ms','ms.kode','anggaran.kode_subdit')
            ->join('users as us','us.id','anggaran.updated_by')
            ->leftJoin('anggaran_subdit as as',function ($e){
                return $e->on('as.kode_subdit','anggaran.kode_subdit')
                    ->on('as.tahun','anggaran.tahun');
            })
            ->where('anggaran.active',true)
            ->where('anggaran.flag',true);
        if(\Auth::user()->get_role_name->level == '1'){
            $data = $data->where('anggaran.tahun',$y);
        }else{
            $data = $data->where('anggaran.kode_subdit',\Auth::user()->kode_subdit);
        }
        $data=$data->distinct()->get(['anggaran.*','us.nama as user','ms.nama','as.id as id_as','as.jumlah as pagu']);
        $totalpagu = number_format(AnggaranSubdit::whereIn('id',$data->pluck('id_as')->all())->sum('jumlah'));
        $totalanggaran = number_format(AnggaranDetail::whereIn('id_anggaran',$data->pluck('id')->all())->first(\DB::raw('SUM(harga*var1*var2*var3*var4) as total'))->total);
        $vw = view('anggaran.component',compact('data'))->render();
        return response()->json(['view'=>$vw,'totalpagu'=>$totalpagu,'totalanggaran'=>$totalanggaran]);
    }
}
