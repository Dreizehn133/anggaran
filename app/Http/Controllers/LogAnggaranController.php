<?php

namespace App\Http\Controllers;

use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\AnggaranLog;
use App\Models\MSubdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class LogAnggaranController extends Controller
{
    public function index()
    {
        $subdit = MSubdit::where('active',true);
        if(Auth::user()->get_role_name->level != '1'){
            $subdit = $subdit->where('kode',Auth::user()->kode_subdit);
        }
        $subdit = $subdit->get();
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));

        return view('log_anggaran.index',compact('years','subdit'));
    }
    function data(Request $request){
        $data = AnggaranLog::join('anggaran_detail','anggaran_detail.id','anggaran_log.id_anggaran_detail')
            ->join('anggaran','anggaran.id','anggaran_detail.id_anggaran')
            ->join('m_subdit as ms','ms.kode','anggaran.kode_subdit')
            ->leftJoin('users as us','us.id','anggaran_log.created_by')
            ->where('anggaran.active',true)
            ->where('anggaran_detail.active',true)
            ->where('anggaran.tahun',$request->tahun)
            ->where('anggaran.kode_subdit','like',$request->subdit.'%')
            ->orderBy('anggaran.kode_subdit')->orderBy('anggaran_detail.kode_gl')->orderBy('anggaran_log.created_at')->select(['anggaran_log.*','anggaran_detail.kode_gl','anggaran_detail.kegiatan','ms.nama','ms.kode','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('sebelumnya',function ($e){
                return number_format($e->sebelumnya);
            })
            ->editColumn('perubahan',function ($e){
                return number_format($e->perubahan);
            })
            ->rawColumns([])->make(true);
    }

}
