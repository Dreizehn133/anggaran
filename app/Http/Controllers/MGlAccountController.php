<?php

namespace App\Http\Controllers;

use App\Models\MGlAccount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class MGlAccountController extends Controller
{
    function index(){
        return view('master.gl_account.index');
    }
    function index2(){
        return view('master.gl_account.index2');
    }
    function data(Request $request){
        $data = MGlAccount::join('users as us','us.id','m_gl_account.created_by')
            ->where('m_gl_account.flag',$request->flags)
            ->orderBy('m_gl_account.kode')
            ->select(['m_gl_account.*','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action',function ($e){
                $es = $e->active ? 'Disable' : 'Enable';
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-warning" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-swatchbook"></i>&nbsp;'.$es.'</button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="removePermanent(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i>&nbsp;</button>';
            })
            ->editColumn('flag',function ($e){
                return $e->flag == '1' ?
                    '<span class="badge badge-pill badge-primary">Header</span>'
                    :'<span class="badge badge-pill badge-secondary">Detail</span>'
                    ;
            })
            ->rawColumns(['action','flag'])->make(true)
            ;
    }
    function header(Request $request){
        $data = MGlAccount::where('flag',true)->where('active',true);
        if(isset($request->is_detail)){
            $data = $data->where('kode','like','%.%.%.%');
        }
        $data = $data->orderBy('kode')->get();
        return response()->json($data);
    }
    function post(Request $request){
        $rules = [
            'nama' => 'required',
            'kode' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if(isset($request->ids)){
            $data = MGlAccount::find(base64_decode($request->ids));
            $data->header_id = $request->header_id;
            $data->nama = $request->nama;
            $data->kode = $request->kode;
            $data->created_by = \Auth::id();
            $data->save();
        }else{
            if($request->flag == 'on'){
                foreach ($request->header_id2 as $hd){
                    $data = new MGlAccount();
                    $header = MGlAccount::find($hd);
                    $kode = $header->kode;
                    $kode.='.'.$request->kode;
                    $data->header_id = $header->id;
                    $data->kode = $kode;
                    $data->flag = $request->flag == 'on'? '0':'1';
                    $data->nama = $request->nama;
                    $data->active = true;
                    $data->created_by = \Auth::id();
                    $data->save();
                }
            }else{
                $data = new MGlAccount();
                $kode = '';
                if($request->header_id != '0'){
                    $header = MGlAccount::find($request->header_id);
                    $kode = $header->kode;
                    $kode.='.'.$request->kode;
                }else{
                    $header = null;
                    $kode = $request->kode;
                }
                $data->header_id = $header != null ? $header->id : null;
                $data->kode = $kode;
                $data->flag = $request->flag == 'on'? '0':'1';
                $data->nama = $request->nama;
                $data->active = true;
                $data->created_by = \Auth::id();
                $data->save();
            }
        }

        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(MGlAccount::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
//        MGlAccount::find($id)->update(['active'=>false]);
        $gl = MGlAccount::find($id);
        $gl->active = $gl->active ? false:true;
        $gl->save();
        return response()->json('Sukses');
    }
    function permanentDelete(Request $request){
        $id=base64_decode($request->ids);
        MGlAccount::find($id)->delete();
        return response()->json('Sukses');
    }
}
