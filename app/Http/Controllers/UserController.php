<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuRole;
use App\Models\MSubdit;
use App\Models\SessionAuth;
use App\Models\User;
use App\Models\UserLog;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function subdomain($subdomain){
        return \redirect('https://ops.devops.anggaran.p4');
//        return $subdomain;
    }
    function index(){
        $role = UserRole::all();
        $subdit = MSubdit::where('active',true)->get();
        return view('master.user.index',compact('role','subdit'));
    }
    function data(Request $request){
        $data = User::where('users.active',true)
            ->join('m_subdit as ms',function ($e){
                return $e->on('ms.kode','users.kode_subdit');
            })
            ->join('user_role as ur',function ($e){
                return $e->on('ur.kode','users.role');
            })
            ->select(['users.*','ur.nama as role','ms.nama as subdit']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action',function ($e){
                return '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>';
            })
            ->addColumn('session',function ($e){
                return '<button class="btn btn-xs btn-warning" onclick="destroySession(\'' . base64_encode($e->id) . '\')"><i class="fa fa-close"></i></button>';
            })
            ->rawColumns(['action','session'])->make(true)
            ;
    }
    function header(){
        return response()->json(
            ['header'=>Menu::where('header',true)->where('active',true)->where('title',false)->get(),
                'title' => Menu::where('title',true)->where('active',true)->get()
            ]
        );
    }
    function post(Request $request){
        Log::debug($request);

        if(isset($request->ids)) {
            $data = User::find(base64_decode($request->ids));
            if(isset($request->password)){
                UserLog::create([
                    'id_user'=>\Auth::id(),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'activity'=>'CHANGE-PASSWORD-BY='.\Auth::user()->nama,
                    'user_agent'=>$request->userAgent(),
                    'ip_address'=>$request->ip(),
                    'ip_client'=>$request->getClientIp()
                ]);
                $data->password = \Hash::make($request->password);
            }
        }else{
            $data = new User();
            $data->password = \Hash::make($request->password);
        }
        $data->nama = $request->name;
        $data->email = $request->email;
        $data->active = true;
        $data->kode_subdit = $request->subdit;
        $data->role = $request->role;
        $data->save();
        return response()->json('Sukses!');
    }
    function detail($id){
        $id=base64_decode($id);
        return response()->json(User::find($id));
    }
    function delete(Request $request){
        $id=base64_decode($request->ids);
        User::find($id)->update(['active'=>false]);
        return response()->json('Sukses');
    }
    function destroySession(Request $request){

        SessionAuth::where('user_id',base64_decode($request->ids))->delete();
        UserLog::create([
            'id_user'=>base64_decode($request->ids),
            'created_at'=>date('Y-m-d H:i:s'),
            'activity'=>'DESTROY-SESSION-BY='.\Auth::user()->nama,
            'user_agent'=>$request->userAgent(),
            'ip_address'=>$request->ip(),
            'ip_client'=>$request->getClientIp()
        ]);
        return response()->json('Sukses');
//        $us = User::where('id',base64_decode($request->ids))->first();
//        $tempUSer = \Auth::user();
//        \Auth::setUser($us);
//        \Auth::logout();;
//        \Auth::setUser($tempUSer);
    }
    function indexUserActivity(){
        return view('master.user_activity.index');
    }
    function dataUserAct(Request $request){
        $data = UserLog::join('users as us','us.id','user_log.id_user')
            ->orderBy('user_log.created_at')
            ->select(['user_log.*','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->rawColumns([])->make(true)
            ;
    }

}
