<?php

namespace App\Http\Controllers;

use App\Exports\AnggaranExport;
use App\Models\Anggaran;
use App\Models\AnggaranDetail;
use App\Models\AnggaranDetailCatatan;
use App\Models\AnggaranLog;
use App\Models\AnggaranSubdit;
use App\Models\MGlAccount;
use App\Models\MSubdit;
use App\Models\Pengelola;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\DataTables;

class AnggaranController extends Controller
{
    function index(Request $request){

        if(isset($request->detail)){
            $data = Anggaran::join('m_subdit as ms','ms.kode','anggaran.kode_subdit')->where('anggaran.active',true)
                ->where('anggaran.id',base64_decode($request->detail))->first(['anggaran.*','ms.nama']);
            $data->anggaran = base64_encode($data->id);
            $pagu = AnggaranSubdit::where('kode_subdit',$data->kode_subdit)->where('tahun',$data->tahun)->first();
            $gl = MGlAccount::where('active',true)->whereNull('header_id')->orderBy('kode')->get();
            $pengelola = Pengelola::where('status','<>','0')->orderBy('code')->get();
            return view('anggaran.detail.index',compact('data','gl','pagu','pengelola'));
        }
        $subdit = MSubdit::where('active',true);
        if(Auth::user()->role != env('ROLE_ADMIN')){
            $subdit = $subdit->where('kode',Auth::user()->kode_subdit);
        }
        $subdit = $subdit->get();
        $years = range(date("Y") + config('app.limit_tahun'), date("Y") - config('app.limit_tahun'));

        return view('anggaran.index',compact('years','subdit'));
    }
    function data(Request $request){
        $data = Anggaran::join('m_subdit as ms','ms.kode','anggaran.kode_subdit')
            ->join('users as us','us.id','anggaran.created_by')
            ->where('anggaran.active',true)
            ;
        if(Auth::user()->role == env('ROLE_USER')){
            $data = $data->where('anggaran.kode_subdit',Auth::user()->kode_subdit);
        }else{
            $data = $data->where('anggaran.tahun',$request->tahun);
        }
        $data = $data->orderBy('anggaran.kode_subdit')->orderBy('anggaran.tahun','desc')->select(['anggaran.*','ms.nama','ms.kode','us.nama as user']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('detail',function ($e){
                $btn = '';
                if(Auth::user()->get_role_name->level == '1'){
                    $btn .= '<button class="btn btn-xs btn-danger" onclick="changeStatus(\'' . base64_encode($e->id) . '\')"><i class="fa fa-code"></i></button>&nbsp;';
                }
                if(Auth::user()->get_role_name->level == '1' || $e->flag == '0'){
                    $btn .= '<a class="btn btn-xs btn-primary" href="'.route('anggaran.index',['detail'=>base64_encode($e->id)]).'"><i class="fa fa-bars"></i></a>&nbsp;';
                }
                $cls = $e->flag=='1'?"btn btn-xs btn-success":"btn btn-xs btn-warning";
                $btn .= '<a class="'.$cls.'" href="'.route('anggaran.detail.preview',[base64_encode($e->id)]).'" target="_blank"><i class="fa fa-eye"></i></a>';

                if($e->flag == '1'){
                }
                return $btn;
            })
            ->addColumn('jumlah',function ($e){
                $data = AnggaranDetail::join('m_gl_account as acc','acc.kode','anggaran_detail.kode_gl')->where('anggaran_detail.id_anggaran',$e->id)->where('anggaran_detail.active',true)->first(DB::raw('SUM(anggaran_detail.harga * (anggaran_detail.var1 * anggaran_detail.var2 * anggaran_detail.var3 * anggaran_detail.var4)) as jumlah'));
                return number_format($data->jumlah);
            })
            ->editColumn('flag',function ($e){
                $flg =  $e->flag == '1' ?
                    '<span class="badge badge-pill badge-primary">Selesai</span>'
                    :'<span class="badge badge-pill badge-secondary">Proses</span>';
                $flg .= $e->tor != '0'?'&nbsp;<a class="badge badge-pill badge-success" href="'.route('anggaran.download',['TOR',encrypt($e->id)]).'">TOR <i class="fa fa-check-circle"></i></a>':'&nbsp;<span class="badge badge-pill badge-danger">TOR <i class="fa fa-times-circle"></i></span>';
                $flg .= $e->rab != '0'?'&nbsp;<a class="badge badge-pill badge-success" href="'.route('anggaran.download',['RAB',encrypt($e->id)]).'">RAB <i class="fa fa-check-circle"></i></a>':'&nbsp;<span class="badge badge-pill badge-danger">RAB <i class="fa fa-times-circle"></i></span>';

                return $flg;
            })
            ->rawColumns(['flag','detail'])->make(true);
    }
    function post(Request $request){
        $check = Anggaran::where('kode_subdit',$request->subdit)
            ->where('tahun',$request->thn)->where('active',true)->first();
        if($check != null){
            return response()->json(['thn'=>["Subdit Telah Teregistrasi untuk tahun yg dipilih!"],'subdit'=>['Tahun Telah Teregistrasi']],400);
        }
        $anggaran = new Anggaran();
        $anggaran->tahun = $request->thn;
        $anggaran->kode_subdit = $request->subdit;
        $anggaran->active = true;
        $anggaran->created_by = Auth::id();
        $anggaran->save();
        return response()->json('Sukses');
    }
    function changeStatus(Request $request){
        $id=base64_decode($request->ids);
        \Log::debug($id);
        $ang = Anggaran::where('id',$id)->first();
        if(isset($request->status)){
            $check = AnggaranSubdit::where('kode_subdit',$ang->kode_subdit)
                ->where('tahun',$ang->tahun)->first();
            if($check == null){
                $check = new AnggaranSubdit();
                $check->jumlah = 0;
            }
            $det = AnggaranDetail::where('id_anggaran',$ang->id)->where('active',true)->first(DB::raw('SUM(harga*(var1 * var2 * var3 * var4)) as total'))->total;
            if($det > $check->jumlah){
                return response()->json('Pengeluaran lebih besar dari Pagu!',500);
            }
            $ang->flag = $request->status;
        }else{
            if(!$ang->flag){
                $ang->flag = '1';
            } else {
                $ang->flag = '0';
            }
        }
        $ang->updated_by = Auth::id();
        $ang->save();
        \Log::debug($ang);
        return response()->json('Sukses');
    }

    //detail
    function postDetil(Request $request){
        $rules = [
            'jumlah' => 'required',
            'satuan' => 'required',
            'svar1' => 'required',
            'var1' => 'required',
//            'var2' => 'required',
            'kegiatan' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $pagu = AnggaranSubdit::join('anggaran as ag',function ($e){
            return $e->on('ag.tahun','anggaran_subdit.tahun')
                ->on('ag.kode_subdit','anggaran_subdit.kode_subdit');
        })->where('ag.id',base64_decode($request->anggaran))->first(['anggaran_subdit.*']);
        if($pagu == null){
            return response()->json('Pagu Belum di tambahkan oleh administrator!', 400);
        }
        $agd = AnggaranDetail::where('id_anggaran',base64_decode($request->anggaran))->where('active',true)->first(DB::raw('SUM(harga*(var1 * var2 * var3 * var4)) as total'))->total;
        $act = 'ADD';
        $log = new AnggaranLog();
        $lastData = null;
        if(isset($request->ids)){
            $data = AnggaranDetail::find(base64_decode($request->ids));
            $lastData = ($data->harga * $data->var1 * $data->var2 * $data->var3 * $data->var4);
            $dets = $agd - $lastData;
            if($dets + ($request->jumlah * $request->var1 * $request->var2 * $request->var3 * $request->var4) > $pagu->jumlah){
                return response()->json([
                    'jumlah'=>'Total Anggaran lebih besar dari Pagu!',
                ], 400);
            }
            $act = 'UPDATE';
        }else{
//            $check = AnggaranDetail::where('id_anggaran',base64_decode($request->anggaran))
//                ->where('kode_gl',$request->gl)->first();
//            if($check != null){
//                return response()->json(['gl'=>['GL Telah di Tambahkan!']],400);
//            }else
            if($agd + ($request->jumlah * $request->var1 * $request->var2  * $request->var3 * $request->var4) > $pagu->jumlah){
                return response()->json([
                    'jumlah'=>'Total Pengeluaran lebih besar dari Pagu!',
                ], 400);
            }
            $data = new AnggaranDetail();
        }
        $data->id_anggaran = base64_decode($request->anggaran);
        $data->kode_gl = $request->gl;
        $data->kegiatan = $request->kegiatan;
//        $data->detail = $request->detail;
        $data->satuan = strtoupper($request->satuan);
//        $data->qty = $request->qty;
        $data->harga = $request->jumlah;
        $data->var1 = $request->var1;
        $data->var2 = $request->var2 == null ? 1 : $request->var2;
        $data->satuan_var1 = $request->svar1;
        $data->satuan_var2 = $request->svar2;
        $data->var3 = $request->var3 == null ? 1 : $request->var3;
        $data->var4 = $request->var4 == null ? 1 : $request->var4;
        $data->satuan_var1 = $request->svar1;
        $data->satuan_var2 = $request->svar2;
        $data->satuan_var3 = $request->svar3;
        $data->satuan_var4 = $request->svar4;
        $data->dikelolah_oleh_kp = $request->kp != null?'1':'0';
        $data->dikelolah_oleh_it = $request->it != null?'1':'0';
        $data->iku = $request->iku;
        $data->created_by = Auth::id();
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        $ttalChange = $data->harga * $data->var1 * ($data->var2??1) * ($data->var3??1) * ($data->var4??1);
        AnggaranLog::create([
           'action_user'=>$act,
           'id_anggaran_detail'=>$data->id,
            'created_by'=>Auth::id(),
            'created_at'=>date('Y-m-d H:i:s'),
            'perubahan'=> $ttalChange,
            'sebelumnya'=> $lastData,
        'ref_amount'=> $lastData != null? $ttalChange - $lastData : null
        ]);
        return response()->json('Sukses!');
    }

    function postCatatan(Request $request){
        $rules = [
            'cttn' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
//        $data = AnggaranDetail::find(base64_decode($request->idh));
//        $data->ket = $request->cttn;
//        $data->save();
        AnggaranDetailCatatan::create([
           'anggaran_detail_id'=>base64_decode($request->idh),
            'catatan'=>$request->cttn,
            'created_by'=>Auth::id(),
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        return response()->json('Sukses!');
    }

    function getCatatan($idh){
        return \response()->json(AnggaranDetailCatatan::join('users as us','us.id','anggaran_detail_catatan.created_by')->where('anggaran_detail_catatan.anggaran_detail_id',base64_decode($idh))->orderBy('anggaran_detail_catatan.created_at')->get(['anggaran_detail_catatan.*','us.nama']));
    }

    function dataDetail(Request $request){
        $data = AnggaranDetail::join('m_gl_account as gl','gl.kode','anggaran_detail.kode_gl')
            ->join('anggaran as ag','ag.id','anggaran_detail.id_anggaran')
            ->where('anggaran_detail.id_anggaran',base64_decode($request->anggaran))
            ->where('anggaran_detail.active',true)
            ->orderBy('anggaran_detail.kode_gl')
            ->select(['anggaran_detail.*','gl.nama','ag.flag']);
        return DataTables::of($data)

            ->addIndexColumn()
            ->addColumn('jumlah',function ($e){
                return number_format($e->harga * $e->var1 * $e->var2 * $e->var3* $e->var4);
            })
            ->editColumn('harga',function ($e){
                return number_format($e->harga). ' '.$e->satuan;
            })
            ->editColumn('var1',function ($e){
                return number_format($e->var1) .' '.$e->satuan_var1;
            })
            ->editColumn('var2',function ($e){
                return number_format($e->var2) .' '.$e->satuan_var2;
            })
            ->editColumn('var3',function ($e){
                return number_format($e->var3) .' '.$e->satuan_var3;
            })
            ->editColumn('var4',function ($e){
                return number_format($e->var4) .' '.$e->satuan_var4;
            })
            ->addColumn('qty',function ($e){
                return ($e->var1 * $e->var2 * $e->var3 * $e->var4) .' '.$e->satuan;
            })
            ->addColumn('files',function ($e){
                $btn = '';
                $btn .= $e->kak != '0'?'&nbsp;<a class="badge badge-pill badge-success" target="_blank" href="'.route('anggaran.download.detail',['KAK',base64_encode($e->id)]).'">KAK <i class="fa fa-check-circle"></i></a>':'&nbsp;<button class="badge badge-pill badge-danger" onclick="uploadDetail(\''.'KAK'.'\''.',\''.base64_encode($e->id).'\')">KAK <i class="fa fa-times-circle"></i></button>';
                $btn .= $e->rab != '0'?'&nbsp;<a class="badge badge-pill badge-success" target="_blank" href="'.route('anggaran.download.detail',['RAB',base64_encode($e->id)]).'">RAB <i class="fa fa-check-circle"></i></a>':'&nbsp;<button class="badge badge-pill badge-danger" onclick="uploadDetail(\''.'RAB'.'\''.',\''.base64_encode($e->id).'\')">RAB <i class="fa fa-times-circle"></i></button>';
                return $btn;
            })
            ->editColumn('kode_gl',function ($e){
                return '<b>'.$e->kode_gl .'</b><br/><small>'.$e->nama.'</small>';
            })
            ->filterColumn('anggaran_detail.kode_gl', function($query, $keyword) {
                $query->whereRaw("LOWER(`gl`.`nama`) like ?", ["%{$keyword}%"])
                ->orWhereRaw("LOWER(`anggaran_detail`.`kode_gl`) like ?", ["%{$keyword}%"])
                ;
            })
            ->filterColumn('anggaran_detail.var1', function($query, $keyword) {
                $query->whereRaw("LOWER(`anggaran_detail`.`satuan_var1`) like ?", ["%{$keyword}%"])
                    ->orWhereRaw("`anggaran_detail`.`var1` = ?", [$keyword])
                    ->orWhereRaw("`anggaran_detail`.`satuan` = ?", [$keyword])
                ;
            })
            ->filterColumn('anggaran_detail.var2', function($query, $keyword) {
                $query->whereRaw("LOWER(`anggaran_detail`.`satuan_var2`) like ?", ["%{$keyword}%"])
                    ->orWhereRaw("`anggaran_detail`.`var2` = ?", [$keyword])
                ;
            })
            ->addColumn('action',function ($e){
                if($e->flag == '1'){
                    $btn =  '<span class="badge badge-pill badge-primary">Telah Selesai</span> &nbsp;';
                }else{
                    $btn =  '<button class="btn btn-xs btn-primary" onclick="edit(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i></button>&nbsp;
                    <button class="btn btn-xs btn-danger" onclick="remove(\'' . base64_encode($e->id) . '\')"><i class="fa fa-trash"></i></button>&nbsp;';
                }
                if(Auth::user()->get_role_name->level == '1'){
                    $btn .= '<button class="btn btn-xs btn-success" onclick="editCatatan(\'' . base64_encode($e->id) . '\')"><i class="fa fa-edit"></i> &nbsp;Catatan</button>';
                }
                return $btn;
            })
            ->smart(true)
            ->rawColumns(['action','kode_gl','files'])->make(true);
    }

    function detailDetail($id){
        return response()->json(AnggaranDetail::find(base64_decode($id)));
    }
    function removeDetail(Request $request){
        $data = AnggaranDetail::where('id',base64_decode($request->ids))->first();
        $data->active= false;
        $data->save();
        AnggaranLog::create([
            'action_user'=>'DELETE',
            'id_anggaran_detail'=>$data->id,
            'created_by'=>Auth::id(),
            'created_at'=>date('Y-m-d H:i:s'),
            'perubahan'=> 0,
            'sebelumnya'=> $data->harga * $data->var1 * ($data->var2??1) * ($data->var3??1) * ($data->var4??1),
            'ref_amount'=> null
        ]);
        return response()->json('Sukses');
    }
    function preview($id){
       $header = Anggaran::join('m_subdit as ms','ms.kode','anggaran.kode_subdit')->where('anggaran.id',base64_decode($id))->first(['anggaran.*','ms.nama','ms.leader','ms.nip','ms.jabatan']);
       $data = AnggaranDetail::join('m_gl_account as gl','gl.kode','anggaran_detail.kode_gl')
           ->where('anggaran_detail.id_anggaran',base64_decode($id))
           ->where('anggaran_detail.active',true)
           ->orderBy('anggaran_detail.kode_gl')
           ->groupBy('anggaran_detail.id')
           ->get(['anggaran_detail.*','gl.nama',DB::raw('SUM(anggaran_detail.harga * anggaran_detail.var1 * anggaran_detail.var2 * anggaran_detail.var3 * anggaran_detail.var4) as jumlah')]);
       $gl = MGlAccount::where('active',true)->orderBy('kode')->get();
       $qr = $header->flag == '1'?base64_encode(QrCode::format('png')->size(150)->generate(route('anggaran.detail.preview',[$id]))):null;
       return view('anggaran.detail.preview',compact('header','data','gl','qr'));
    }
    function getTotalAnggaran($id){
        $id = base64_decode($id);
        return response()->json(AnggaranDetail::where('id_anggaran',$id)->where('active',true)->first(DB::raw('SUM(harga*(var1 * var2 * var3 * var4)) as total'))->total);
    }
    function downloadExcel($id){
        return \Maatwebsite\Excel\Facades\Excel::download(new AnggaranExport($id),'export.xlsx');
    }
    function glChange(Request $request) {
        if($request->ids == null){
            return response()->json(['data'=>[]]);
        }
        $data = MGlAccount::where('header_id',MGlAccount::where('kode',$request->ids)->where('active',true)->first()->id)->get();
        return response()->json(['data'=>$data]);
    }
    function upload(Request $request){
        Log::debug($request);
        $data = Anggaran::find(base64_decode($request->ids));
        $msg = '';
        $files = '';
        $format = $request->file->getClientOriginalExtension();
        if($request->type == 'tor'){
            if(Str::contains(strtolower($format),'pdf')){
                $data->tor = 1;
            }else{
                $data->tor = 2;
            }
            $msg = 'TOR';
            $files = 'tor.'.strtolower($format);
        }else if($request->type == 'rab'){
            if(Str::contains(strtolower($format),'pdf')){
                $data->rab = 1;
            }else{
                $data->rab = 2;
            }
            $msg = 'RAB';
            $files = 'rab.'.strtolower($format);
        }
        if($request->file('file') != null){
            $dest = public_path('berkas'.'/'.$data->tahun.'/'.$msg.'/'.$data->kode_subdit);
            $request->file('file')->move($dest,$files);
        }
        $data->save();
        return response()->json('Sukses Upload '.$msg);
    }
    public function download($jenis,$id){
        $data = Anggaran::find(decrypt($id));
        if(strtolower($jenis) == 'tor'){
            if($data->tor == '1'){
                $files = 'tor.pdf';
            }else{
                $files = 'tor.docx';
            }
        }else if(strtolower($jenis)== 'rab'){
            if($data->rab == '1'){
                $files = strtolower($jenis).'.pdf';
            }else{
                $files = 'rab.docx';
            }
        }
        return  response()->download(public_path('berkas'.'/'.$data->tahun.'/'.$jenis.'/'.$data->kode_subdit.'/'.$files),$data->kode_subdit.'_'.$data->tahun.'_'.$files);
//        return  Response::stream(public_path('berkas'.'/'.$data->tahun.'/'.$jenis.'/'.$data->kode_subdit.'/'.$files));
    }
    function uploadDetail(Request $request){
        $data = AnggaranDetail::find(base64_decode($request->ids));
        $msg = '';
        $files = '';
        $format = $request->file->getClientOriginalExtension();
        if($request->type == 'KAK'){
            if(Str::contains(strtolower($format),'pdf')){
                $data->kak = 1;
            }else{
                $data->kak = 2;
            }
            $msg = 'KAK';
            $files = 'kak.'.strtolower($format);
        }else if($request->type == 'RAB'){
            if(Str::contains(strtolower($format),'pdf')){
                $data->rab = 1;
            }else{
                $data->rab = 2;
            }
            $msg = 'RAB';
            $files = 'rab.'.strtolower($format);
        }
        if($request->file('file') != null){
            $dest = public_path('berkas'.'/detail/'.$data->kode_gl.'/'.$request->ids.'/'.$msg);
            $request->file('file')->move($dest,$files);
        }
        $data->save();
        return response()->json('Sukses Upload '.$msg);
    }
    public function downloadDetail($jenis,$id){
        $data = AnggaranDetail::find(base64_decode($id));
        if(strtolower($jenis) == 'kak'){
            if($data->kak == '1'){
                $files = 'kak.pdf';
            }else{
                $files = 'kak.docx';
            }
        }else if(strtolower($jenis)== 'rab'){
            if($data->rab == '1'){
                $files = 'rab.pdf';
            }else{
                $files = 'rab.docx';
            }
        }
        return  response()->download(public_path('berkas'.'/detail/'.$data->kode_gl.'/'.$id.'/'.$jenis.'/'.$files),$files);
    }
}
