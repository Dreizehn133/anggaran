<?php

namespace App\Http\Livewire;

use App\Models\MGlAccount;
use Livewire\Component;

class GlAccount extends Component
{
    public $data,$id_gl,$kode,$nama,$flag,$header_id='0',$data_header;
    public $updateMode = false;
    public function render()
    {
        $this->data = MGlAccount::where('m_gl_account.active',true)->join('users as us','us.id','m_gl_account.created_by')->get(['m_gl_account.*','us.nama as user']);
        $this->data_header = MGlAccount::where('m_gl_account.active',true)->where('flag',true)->get();
        return view('livewire.gl-account');
    }
    private function resetInputFields(){
        $this->nama = '';
        $this->kode = '';
        $this->id_gl = '';
        $this->flag = '';
    }

    public function store()
    {
        \Log::debug('begin store');
        $this->emit('datatable');
        $this->dispatchBrowserEvent('refreshComponent', ['componentName' => '#myDataTable']);

        $validatedDate = $this->validate([
            'nama' => 'required',
            'kode' => 'required',
        ]);
        $kode = '';
        $flag = '1';
        \Log::debug($this->header_id);
        if($this->header_id != '0'){
            $kode = MGlAccount::find($this->header_id)->kode;
            $kode .= '.';
            $flag = '1';
        }
        $account = new MGlAccount();
        $account->kode = $kode.$this->kode;
        $account->nama = $this->nama;
        $account->flag = $flag;
        $account->active = true;
        $account->created_by = \Auth::id();
        $account->save();

//        session()->flash('message', 'Users Created Successfully.');

        $this->resetInputFields();

        $this->emit('modal');
    }

    public function edit($id)
    {
        $this->updateMode = true;
        $data = MGlAccount::where('id',$id)->first();
        $this->id_gl = $id;
        $this->nama = $data->nama;
        $this->kode = $data->kode;

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function update()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        if ($this->user_id) {
            $user = User::find($this->user_id);
            $user->update([
                'name' => $this->name,
                'email' => $this->email,
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Users Updated Successfully.');
            $this->resetInputFields();

        }
    }

    public function delete($id)
    {
        if($id){
            User::where('id',$id)->delete();
            session()->flash('message', 'Users Deleted Successfully.');
        }
    }
}
