<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('{subdomain}.anggaran.p4')->group(function () {
    Route::get('/subdomain', [App\Http\Controllers\UserController::class, 'subdomain']);
});
Route::get('/', function () {
    return view('auth.login_page');
})->middleware('guest');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/{y}',[\App\Http\Controllers\HomeController::class,'getFilterDashboard']);
Route::group(['prefix' => '/developer', 'middleware' => ['auth']], function () {
    Route::group(['prefix' => '/menu'], function () {
        Route::get('/',[\App\Http\Controllers\MenuController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\MenuController::class,'data'])->name('developer.menu.data');
        Route::get('/header',[\App\Http\Controllers\MenuController::class,'header'])->name('developer.menu.header');
        Route::get('/header/{id}',[\App\Http\Controllers\MenuController::class,'detail'])->name('developer.menu.detail');
        Route::post('/data',[\App\Http\Controllers\MenuController::class,'post'])->name('developer.menu.add');
        Route::post('/delete',[\App\Http\Controllers\MenuController::class,'delete'])->name('developer.menu.delete');
    });
    Route::group(['prefix' => '/role_menu'], function () {
        Route::get('/',[\App\Http\Controllers\MenuRoleController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\MenuRoleController::class,'data'])->name('developer.menu_role.data');
        Route::get('/header',[\App\Http\Controllers\MenuRoleController::class,'header'])->name('developer.menu_role.header');
        Route::get('/header/{id}',[\App\Http\Controllers\MenuRoleController::class,'detail'])->name('developer.menu_role.detail');
        Route::post('/data',[\App\Http\Controllers\MenuRoleController::class,'post'])->name('developer.menu_role.add');
        Route::post('/delete',[\App\Http\Controllers\MenuRoleController::class,'delete'])->name('developer.menu_role.delete');
    });
});

Route::group(['prefix' => '/master', 'middleware' => ['auth']], function () {
    Route::group(['prefix' => '/header_program'], function () {
        Route::get('/',[\App\Http\Controllers\MGlAccountController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\MGlAccountController::class,'data'])->name('master.gl_account.data');
        Route::get('/header',[\App\Http\Controllers\MGlAccountController::class,'header'])->name('master.gl_account.header');
        Route::get('/header/{id}',[\App\Http\Controllers\MGlAccountController::class,'detail'])->name('master.gl_account.detail');
        Route::post('/data',[\App\Http\Controllers\MGlAccountController::class,'post'])->name('master.gl_account.add_data');
        Route::post('/delete',[\App\Http\Controllers\MGlAccountController::class,'delete'])->name('master.gl_account.delete');
        Route::post('/delete_permanent',[\App\Http\Controllers\MGlAccountController::class,'permanentDelete'])->name('master.gl_account.permanentDelete');
    });
    Route::group(['prefix' => '/detail_program'], function () {
        Route::get('/',[\App\Http\Controllers\MGlAccountController::class,'index2']);
    });
    Route::group(['prefix' => '/subdit'], function () {
        Route::get('/',[\App\Http\Controllers\MSubditController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\MSubditController::class,'data'])->name('master.subdit.data');
        Route::get('/header',[\App\Http\Controllers\MSubditController::class,'header'])->name('master.subdit.header');
        Route::get('/header/{id}',[\App\Http\Controllers\MSubditController::class,'detail'])->name('master.subdit.detail');
        Route::post('/data',[\App\Http\Controllers\MSubditController::class,'post'])->name('master.subdit.add_data');
        Route::post('/delete',[\App\Http\Controllers\MSubditController::class,'delete'])->name('master.subdit.delete');
    });
    Route::group(['prefix' => '/user'], function () {
        Route::get('/',[\App\Http\Controllers\UserController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\UserController::class,'data'])->name('master.user.data');
        Route::get('/header',[\App\Http\Controllers\UserController::class,'header'])->name('master.user.header');
        Route::get('/header/{id}',[\App\Http\Controllers\UserController::class,'detail'])->name('master.user.detail');
        Route::post('/data',[\App\Http\Controllers\UserController::class,'post'])->name('master.user.add');
        Route::post('/delete',[\App\Http\Controllers\UserController::class,'delete'])->name('master.user.delete');
        Route::post('/destroy',[\App\Http\Controllers\UserController::class,'destroySession'])->name('master.user.destroy');
    });
    Route::group(['prefix' => '/user_activiy'],function(){
        Route::get('/',[\App\Http\Controllers\UserController::class,'indexUserActivity'])->name('user_activity.index');
        Route::get('/data',[\App\Http\Controllers\UserController::class,'dataUserAct'])->name('user_activity.data');
    });
//    Route::view('gl_account','master.gl_account.index');
});
Route::group(['prefix' => '/anggaran', 'middleware' => ['auth']], function () {
    Route::group(['prefix' => '/list'], function () {
        route::get('/',[\App\Http\Controllers\AnggaranController::class,'index'])->name('anggaran.index');
        route::get('/data',[\App\Http\Controllers\AnggaranController::class,'data'])->name('anggaran.data');
        route::get('/data/{id}',[\App\Http\Controllers\AnggaranController::class,'getTotalAnggaran'])->name('anggaran.total');
        route::post('/data',[\App\Http\Controllers\AnggaranController::class,'post'])->name('anggaran.post');
        route::post('/change',[\App\Http\Controllers\AnggaranController::class,'changeStatus'])->name('anggaran.change');

        Route::group(['prefix'=>'detail'],function (){
            route::get('preview/{id}',[\App\Http\Controllers\AnggaranController::class,'preview'])->name('anggaran.detail.preview');
            route::get('excel/{id}',[\App\Http\Controllers\AnggaranController::class,'downloadExcel'])->name('anggaran.detail.excel');
            route::post('/post',[\App\Http\Controllers\AnggaranController::class,'postDetil'])->name('anggaran.detail.post');
            route::post('/post_catatan',[\App\Http\Controllers\AnggaranController::class,'postCatatan'])->name('anggaran.catatan.post');
            route::get('/data/get_catatan/{idh}',[\App\Http\Controllers\AnggaranController::class,'getCatatan'])->name('anggaran.catatan.get');
            route::post('/remove',[\App\Http\Controllers\AnggaranController::class,'removeDetail'])->name('anggaran.detail.remove');
            route::get('/data',[\App\Http\Controllers\AnggaranController::class,'dataDetail'])->name('anggaran.detail.data');
            route::get('/data/{id}',[\App\Http\Controllers\AnggaranController::class,'detailDetail'])->name('anggaran.detail.detail');
            route::get('/gl_account',[\App\Http\Controllers\AnggaranController::class,'glChange'])->name('anggaran.gl');
            route::post('/upload',[\App\Http\Controllers\AnggaranController::class,'upload'])->name('anggaran.upload');
            route::post('/upload_detail',[\App\Http\Controllers\AnggaranController::class,'uploadDetail'])->name('anggaran.upload.detail');
            route::get('/download/{jenis}/{id}',[\App\Http\Controllers\AnggaranController::class,'download'])->name('anggaran.download');
            route::get('/download_detail/{jenis}/{id}',[\App\Http\Controllers\AnggaranController::class,'downloadDetail'])->name('anggaran.download.detail');
        });
    });
    Route::group(['prefix' => '/pagu'], function () {
        Route::get('/',[\App\Http\Controllers\AnggaranPaguController::class,'index']);
        Route::get('/data',[\App\Http\Controllers\AnggaranPaguController::class,'data'])->name('anggaran.pagu.data');
        Route::get('/header',[\App\Http\Controllers\AnggaranPaguController::class,'header'])->name('anggaran.pagu.header');
        Route::get('/header/{id}',[\App\Http\Controllers\AnggaranPaguController::class,'detail'])->name('anggaran.pagu.detail');
        Route::post('/data',[\App\Http\Controllers\AnggaranPaguController::class,'post'])->name('anggaran.pagu.add');
        Route::post('/delete',[\App\Http\Controllers\AnggaranPaguController::class,'delete'])->name('anggaran.pagu.delete');
    });
    Route::group(['prefix' => '/report_rka'],function(){
        route::get('/',[\App\Http\Controllers\RKAReportController::class,'index'])->name('report_rka.index');
        route::get('/data',[\App\Http\Controllers\RKAReportController::class,'data'])->name('report_rka.data');
    });
    Route::group(['prefix' => '/log_anggaran'],function(){
        route::get('/',[\App\Http\Controllers\LogAnggaranController::class,'index'])->name('log_anggaran.index');
        route::get('/data',[\App\Http\Controllers\LogAnggaranController::class,'data'])->name('log_anggaran.data');
    });
    Route::group(['prefix' => '/rpd'],function(){
        route::get('/',[\App\Http\Controllers\AnggaranRPDsController::class,'index'])->name('rpd.index');
        route::get('/data',[\App\Http\Controllers\AnggaranRPDsController::class,'data'])->name('rpd.data');
        route::post('/data',[\App\Http\Controllers\AnggaranRPDsController::class,'post'])->name('rpd.post');
        route::get('/data_rpd',[\App\Http\Controllers\AnggaranRPDsController::class,'dataRPD'])->name('rpd.detail.data');
        route::post('/data_rpd',[\App\Http\Controllers\AnggaranRPDsController::class,'removeRPD'])->name('rpd.detail.remove');
        route::post('/data_rpd/upload',[\App\Http\Controllers\AnggaranRPDsController::class,'upload'])->name('rpd.upload');
        route::get('/download/{id}',[\App\Http\Controllers\AnggaranRPDsController::class,'download'])->name('rpd.download');
    });
});

Route::group(['prefix' => '/pengumuman', 'middleware' => ['auth']], function () {
    Route::post('data',[\App\Http\Controllers\PengumumenController::class,'store'])->name('pengumuman.post');
    Route::get('data/{id}',[\App\Http\Controllers\PengumumenController::class,'detail'])->name('pengumuman.detail');
    Route::get('data',[\App\Http\Controllers\PengumumenController::class,'data'])->name('pengumuman.data');
});



